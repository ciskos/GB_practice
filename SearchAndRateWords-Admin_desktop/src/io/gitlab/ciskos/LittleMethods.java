/**
 * 
 */
package io.gitlab.ciskos;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.commons.io.FilenameUtils;

/**
 * @author user
 *
 */
public class LittleMethods {
	
	/**
	 * 
	 */
	public static void btnCsv_AL(JButton btnCsv, JTable table) {
		btnCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = LittleMethods.filePath();
				LittleMethods.exportToCSV(table, file);
			}
		});
	}
	
	/**
	 * @return
	 */
	public static JComboBox<String> rsToCB(ResultSet rs) throws SQLException {
		JComboBox<String> comboBox = new JComboBox<>();
		String[] list = null;
		int rows = 0;

		if (rs.last()) {
			rows = rs.getRow();
			rs.beforeFirst();
			list = new String[rows];
		}

		if (rs != null) {
			int i = 0;
			while (rs.next()) {
				list[i] = rs.getString(2);
				i++;
			}
		}
		
		comboBox.setModel(new DefaultComboBoxModel<String>(list));
		return comboBox;
	}
	
	/**
	 * @param rs
	 * @param tableHeader
	 * @throws SQLException
	 */
	public static DefaultTableModel rsToDTM(ResultSet rs, String[] tableHeader) throws SQLException {
		if (!rs.isBeforeFirst()) return null;
		
		int columns = rs.getMetaData().getColumnCount();
		int rows = 0;
		
		if (rs.last()) {
			rows = rs.getRow();
			rs.beforeFirst();
		}
		
		Object[][] tableObject = new Object[rows][columns];

		int i = 0;
		while (rs.next()) {
			for (int j = 1; j <= columns; j++) {
				tableObject[i][j - 1] = rs.getString(j);
			}
			i++;
		}
		
		return new DefaultTableModel(tableObject, tableHeader);
	}
	
	/**
	 * @return
	 * @throws HeadlessException
	 */
	public static File filePath() throws HeadlessException {
		JFileChooser jfc = new JFileChooser();
		
		FileFilter filter = new FileNameExtensionFilter("CSV File", "csv");
		jfc.setFileFilter(filter);
		jfc.addChoosableFileFilter(filter);
		jfc.showSaveDialog(null);
		
		File file = jfc.getSelectedFile();
		if (FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("csv")) {
		    // filename is OK as-is
		} else {
		    file = new File(file.toString() + ".csv");  // append .xml if "foo.jpg.xml" is OK
//				    file = new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName())+".xml"); // ALTERNATIVELY: remove the extension (if any) and replace it with ".xml"
		}
		return file;
	}

	public static boolean exportToCSV(JTable tableToExport, File pathToExportTo) {

	    try {

	        TableModel model = tableToExport.getModel();
	        FileWriter csv = new FileWriter(pathToExportTo);

	        for (int i = 0; i < model.getColumnCount(); i++) {
	            csv.write(model.getColumnName(i) + ",");
	        }

	        csv.write("\n");

	        for (int i = 0; i < model.getRowCount(); i++) {
	            for (int j = 0; j < model.getColumnCount(); j++) {
	                csv.write(model.getValueAt(i, j).toString() + ",");
	            }
	            csv.write("\n");
	        }

	        csv.close();
	        return true;
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return false;
	}
}
