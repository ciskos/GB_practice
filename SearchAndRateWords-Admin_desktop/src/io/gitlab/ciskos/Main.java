package io.gitlab.ciskos;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import io.gitlab.ciskos.gui.Authorization;
import io.gitlab.ciskos.gui.MainWindow;

public class Main {

	
	private static MainWindow window = new MainWindow();

	private static String token = null;
	private static String userID = "1";
	private static String userName = "Admin1";
	private static Authorization dialog;
//	private static String errorLogOrPass = "Неверные логин или пароль, попробуйте ещё раз";
//	private static String errorConnectServer = "Ошибка соединения c сервером\n";
//	private static String errorMsg = "Ошибка";
//	private static int tokenUpdateTime = 50000;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
				try {
//					dialog = new Authorization();
//					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//					dialog.setVisible(true);
//					System.out.println(RestRequest.sitesRequest(RestRequest.SITES));
//					System.out.println(RestRequest.usersRequest(RestRequest.USERS));

					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		});
		
		// TODO пока что глючная функция, надо переделать
/*		try {
			keepConnectionAlive();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
	}

/*	public static void keepConnectionAlive() throws InterruptedException {
		Runnable r = () -> {
			while (true) {
				try {
					if (window != null) {
						StringBuffer authKeepAlive = RestRequest.keepAlive(RestRequest.AUTH, token);
						JSONObject keepAliveResult = new JSONObject(authKeepAlive.toString());
						String success = keepAliveResult.get("success").toString();

						if (success.equals(tru)) {
							System.out.println(authKeepAlive.toString());
						} else {
							JOptionPane.showMessageDialog(null, errorConnectServer
														, errorMsg, JOptionPane.OK_OPTION);
							dialog.setVisible(true);
						}
						
						System.out.println(authKeepAlive.toString());
						Thread.sleep(tokenUpdateTime);
					} else {
						System.out.println(window);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		Thread keepAlive = new Thread(r);
		keepAlive.start();
		keepAlive.join();
	}*/
	
	/**
	 * @return the token
	 */
	public static String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public static void setToken(String token) {
		Main.token = token;
	}

	/**
	 * @return the userID
	 */
	public static String getUserID() {
		return userID;
	}

	/**
	 * @param userID the userID to set
	 */
	public static void setUserID(String userID) {
		Main.userID = userID;
	}

	/**
	 * @return the userName
	 */
	public static String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public static void setUserName(String userName) {
		Main.userName = userName;
	}

	/**
	 * @return the window
	 */
	public static MainWindow getWindow() {
		return window;
	}
}
