package io.gitlab.ciskos.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

//import sun.net.www.protocol.https.Handler;

public class RestRequest {

	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
	private static final String HOST = "https://localhost:5050/v1/";
	public static final String AUTH = HOST + "auth";
	public static final String USERS = HOST + "users";
//	public static final String USERS_ID = HOST + "users";
	public static final String PERSONS = HOST + "persons";
//	public static final String PERSONS_ID = HOST + "persons";
//	public static final String PERSONS_RANK = HOST + "persons";
//	public static final String PERSONS_RANK_ID = HOST + "persons";
//	public static final String PERSONS_RANK_ID_DATE_FROM_TILL = HOST + "persons";
	public static final String SITES = HOST + "sites";
//	public static final String SITES_ID = HOST + "sites";
	

	public static StringBuffer authRequest(String url, String login, String password) throws Exception {
		DataOutputStream wr;
		int responseCode;
		BufferedReader in;
		String inputLine;
		URL obj = new URL(url);
//		URL obj = new URL(null, url, new Handler());
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		StringBuffer response = new StringBuffer();
		String urlParameters = "{\"user\":\"" + login + "\",\"password\":\"" + password + "\"}";

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json");
		
		// Send post request
		con.setDoOutput(true);
		
		wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		responseCode = con.getResponseCode();

		in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		return response;
	}

	public static StringBuffer createUser(String url, String json) throws Exception {
		DataOutputStream wr;
		int responseCode;
		BufferedReader in;
		String inputLine;
		URL obj = new URL(url);
//		URL obj = new URL(null, url, new Handler());
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		StringBuffer response = new StringBuffer();
		String urlParameters = json;

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json");
		
		// Send post request
		con.setDoOutput(true);
		
		wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		responseCode = con.getResponseCode();

		in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		return response;
	}
	
	public static StringBuffer editUser(String url, String json) throws Exception {
		DataOutputStream wr;
		int responseCode;
		BufferedReader in;
		String inputLine;
		URL obj = new URL(url);
//		URL obj = new URL(null, url, new Handler());
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		StringBuffer response = new StringBuffer();
		String urlParameters = json;

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json");
		
		// Send post request
		con.setDoOutput(true);
		
		wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		responseCode = con.getResponseCode();

		in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		return response;
	}
	public static StringBuffer keepAlive(String url, String token) throws Exception {
		DataOutputStream wr;
		int responseCode;
		BufferedReader in;
		String inputLine;
		URL obj = new URL(url);
//		URL obj = new URL(null, url, new Handler());
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		StringBuffer response = new StringBuffer();
		String urlParameters = "{\"token_auth\":\"" + token + "\"}";

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json");
		
		// Send post request
		con.setDoOutput(true);
		
		wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		responseCode = con.getResponseCode();

		in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		return response;
	}
	
	public static StringBuffer getData(String url) throws Exception {

		int responseCode;
		BufferedReader in;
		String inputLine;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		StringBuffer response = new StringBuffer();

		// GET options
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);

		responseCode = con.getResponseCode();

		in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response;
	}	


	// HTTP GET request
//	public static StringBuffer sendGet(String url) throws Exception {
//
//		int responseCode;
//		BufferedReader in;
//		String inputLine;
//		URL obj = new URL(url);
//		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//		StringBuffer response = new StringBuffer();
//
//		// GET options
//		con.setRequestMethod("GET");
//		con.setRequestProperty("User-Agent", USER_AGENT);
//
//		responseCode = con.getResponseCode();
//
//		in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//
//		//print result
////		System.out.println("\nSending 'GET' request to URL : " + url);
////		System.out.println("Response Code : " + responseCode);
////		System.out.println(response.toString());
//
//		return response;
//	}


	
	// HTTP POST request
//	public static StringBuffer sendPost(String url) throws Exception {
////		System.out.println(url);
//		URL obj = new URL(url);
////		URL obj = new URL(null, url, new Handler());
//		
//		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
//		DataOutputStream wr;
//		int responseCode;
//		BufferedReader in;
//		String inputLine;
//		StringBuffer response = new StringBuffer();
//		String urlParameters = "{\"user\":\"Admin2\",\"password\":\"2222\"}";
////		String urlParameters = "user=Admin1&password=1111";
//
//		//add reuqest header
//		con.setRequestMethod("POST");
//		con.setRequestProperty("User-Agent", USER_AGENT);
//		con.setRequestProperty("Content-Type", "application/json");
////		con.addRequestProperty("user", "Admin1");
////		con.addRequestProperty("password", "1111");
//
////		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
////		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
//		
//		// Send post request
//		con.setDoOutput(true);
//		
//		wr = new DataOutputStream(con.getOutputStream());
//		wr.writeBytes(urlParameters);
//		wr.flush();
//		wr.close();
//
//		responseCode = con.getResponseCode();
//
//		in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//		
//		//print result
//		System.out.println("\nSending 'POST' request to URL : " + url);
//		System.out.println("Post parameters : " + urlParameters);
//		System.out.println("Response Code : " + responseCode);
//		System.out.println(response.toString());
//
//		return response;
//	}
	
	
//	public StringBuilder getData(String link) {
////		Http
//		StringBuilder sb = new StringBuilder();
//		InputStream is = null;
//		
//		try {
//			URL url = new URL(link);
//			is = url.openStream();
//			int b;
//			
//			while ((b = is.read()) != -1) {
//				sb.append((char)b);
//			}
//			
//			return sb;
//		} catch (IOException e) {
//			// TODO: handle exception
//			return sb;
//		}
//	}
}
