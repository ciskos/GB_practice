package io.gitlab.ciskos.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {

	// Переменные для подключения к СУБД
//	private static final String CONNECTION = "jdbc:mariadb://localhost:3303/searchandratewords";
	private static final String CONNECTION = "jdbc:mariadb://localhost:3306/searchandratewords";
	private static final String USER = "root";
//	private static final String PASSWORD = "";
	private static final String PASSWORD = "fkg7h";
//    private static Connection connection = null;
    
    // Метод создания подключения к СУБД
    public Connection connect() throws SQLException {
		return DriverManager.getConnection(CONNECTION, USER, PASSWORD);
    }

    // Метод отключения от СУБД
//    public void disconnect() throws SQLException{
//        connection.close();
//    }
}
