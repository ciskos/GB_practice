package io.gitlab.ciskos.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBDelete {
	private Connection connection = null;
	private DBConnect dbc = null;

    public static final String DELETE_USER = "DELETE FROM users WHERE id=?";
    public static final String DELETE_URL_FROM_PAGES = "DELETE FROM pages WHERE siteID=?";
    public static final String DELETE_SITE = "DELETE FROM sites WHERE id=?";

    public static final String DELETE_KEYWORD = "DELETE FROM keywords WHERE ID=?";
	public static final String DELETE_KEYWORDS = "DELETE FROM keywords WHERE personID=?";

	public static final String DELETE_PERSON = "DELETE FROM persons WHERE id=?";
	public static final String DELETE_PERSONSPAGERANK_PAGES = "DELETE ppr"
															+ " FROM personspagerank AS ppr"
															+ " LEFT JOIN pages AS p ON ppr.personID=p.ID"
															+ " WHERE p.siteID=?";
	public static final String DELETE_PERSONSPAGERANK_PERSONS = "DELETE FROM personspagerank WHERE personID=?";

    public DBDelete() {
		this.dbc = new DBConnect();
	}

	public void delete(String query, String value) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(query);
		pStatement.setString(1, value);
		pStatement.executeUpdate();
		connection.close();
	}
    
/*	public void deleteUser(String id) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_USER);
		pStatement.setString(1, id);
		pStatement.executeUpdate(); 
	}

	public void deleteSite(String siteToDelete) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_SITE);
		pStatement.setString(1, siteToDelete);
		pStatement.executeUpdate(); 
	}

	public void deleteURLFromPages(String siteID) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_URL_FROM_PAGES);
		pStatement.setString(1, siteID);
		pStatement.executeUpdate(); 
	}

	public void deleteKeywords(String personID) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_KEYWORDS);
		pStatement.setString(1, personID);
		pStatement.executeUpdate(); 
	}

	public void deletePersons(String personToDelete) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_PERSON);
		pStatement.setString(1, personToDelete);
		pStatement.executeUpdate(); 
	}

	public void deletePersonsPageRankPage(String siteID) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_PERSONSPAGERANK_PAGES);
		pStatement.setString(1, siteID);
		pStatement.executeUpdate(); 
	}
	
	public void deletePersonsPageRankPerson(String personID) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_PERSONSPAGERANK_PERSONS);
		pStatement.setString(1, personID);
		pStatement.executeUpdate(); 
	}

	public void deleteKeyword(String keywordToDelete) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(DELETE_KEYWORD);
		pStatement.setString(1, keywordToDelete);
		pStatement.executeUpdate(); 
	}*/
}
