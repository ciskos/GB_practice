package io.gitlab.ciskos.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBInsert {
	private Connection connection = null;
	private DBConnect dbc = null;

    private static final String INSERT_USER = "INSERT INTO users(login, password, email, isAdmin, parentID)"
    										+ " VALUES(?, ?, ?, ?, ?)";
    private static final String INSERT_SITE = "INSERT INTO sites(name, siteDescription, addedBy)"
    										+ " VALUES(?, ?, ?)";
    private static final String INSERT_PAGE = "INSERT INTO pages(url, siteID)"
											+ " VALUES(?, ?)";
    private static final String INSERT_PERSON = "INSERT INTO persons(name, addedBy)"
											+ " VALUES(?, ?)";
	private static final String INSERT_KEYWORD = "INSERT INTO keywords(name, personID)"
											+ " VALUES(?, ?)";


	private static final String LOG_UPDATE =  "INSERT INTO log(action, adminID)"
											+ " VALUES(?, ?)";

    
    public DBInsert() {
		this.dbc = new DBConnect();
	}

	public void insertUser(String login, String password, String email, int isAdmin, String parentID) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(INSERT_USER);
		pStatement.setString(1, login);
		pStatement.setString(2, password);
		pStatement.setString(3, email);
		pStatement.setInt(4, isAdmin);
		pStatement.setString(5, parentID);

		pStatement.executeUpdate();
		connection.close();
	}

	public void insertSite(String site, String description, String userID) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(INSERT_SITE);
		pStatement.setString(1, site);
		pStatement.setString(2, description);
		pStatement.setString(3, userID);

		pStatement.executeUpdate();
		connection.close();
	}

	public void insertPages(String url, String siteID) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(INSERT_PAGE);
		pStatement.setString(1, url);
		pStatement.setString(2, siteID);
		
		pStatement.executeUpdate();
		connection.close();
	}

	public void insertPerson(String personName, String userID) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(INSERT_PERSON);
		pStatement.setString(1, personName);
		pStatement.setString(2, userID);

		pStatement.executeUpdate();
		connection.close();
	}

	public void insertKeyword(String personID, String keyword) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(INSERT_KEYWORD);
		pStatement.setString(1, keyword);
		pStatement.setString(2, personID);

		pStatement.executeUpdate();
		connection.close();
	}

	public void logAdd(String message, String adminID) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(LOG_UPDATE);
		pStatement.setString(1, message);
		pStatement.setString(2, adminID);

		pStatement.executeUpdate();
		connection.close();
	}
}
