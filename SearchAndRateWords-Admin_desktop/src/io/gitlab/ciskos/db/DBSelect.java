package io.gitlab.ciskos.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBSelect {
	private Connection connection = null;
	private DBConnect dbc = null;

    public static final String AUTH = "SELECT id, login, password FROM users WHERE login=? AND password=?";
    public static final String PAGES = "SELECT * FROM pages WHERE siteID=?";
    public static final String PAGES_LIST = "SELECT id, url FROM pages";
    public static final String PAGES_ALL = "SELECT COUNT(siteID) FROM pages WHERE siteID=? GROUP BY siteID";
    public static final String PAGES_NOT_SCANNED =
    		"SELECT COUNT(siteID) FROM pages WHERE siteID=? AND lastScanDate IS NULL GROUP BY siteID";
    public static final String PAGES_SCANNED =
    		"SELECT COUNT(siteID) FROM pages WHERE siteID=? AND lastScanDate IS NOT NULL GROUP BY siteID";
    
    public static final String USERS = "SELECT id, isAdmin, login, email FROM users WHERE parentID=?";
    public static final String USERS_CHECK_UPDATE = "SELECT id, isAdmin, login, email FROM users WHERE ID=?";
    public static final String SITES = "SELECT id, name, siteDescription FROM sites WHERE addedBy=?";
    public static final String SITES_LIST = "SELECT id, name FROM sites";
    public static final String SITES_CHECK_UPDATE = "SELECT id, name, siteDescription FROM sites WHERE ID=?";
    public static final String SITES_ID_FOR_PAGES = "SELECT id FROM sites WHERE name=?";
    public static final String SITES_FOR_PERSON = "SELECT s.ID, s.name"
										    		+ " FROM persons AS pr"
										    		+ " RIGHT JOIN personspagerank AS ppr ON pr.ID=ppr.personID"
										    		+ " LEFT JOIN pages AS pg ON ppr.PageID=pg.ID"
										    		+ " LEFT JOIN sites AS s ON pg.siteID=s.ID"
										    		+ " WHERE pr.ID=?";
	public static final String PERSONS = "SELECT id, name FROM persons WHERE addedBy=?";
	public static final String PERSONS_LIST = "SELECT id, name FROM persons";
	public static final String PERSON_CHECK_UPDATE = "SELECT id, name FROM persons WHERE ID=?";
	public static final String PERSON_KEYWORDS = "SELECT k.ID, k.name"
													+ " FROM keywords AS k"
													+ " LEFT JOIN persons AS p ON k.personID=p.ID WHERE p.ID=?";
	public static final String KEYWORD_CHECK_UPDATE = "SELECT id, name FROM keywords WHERE ID=?";
	public static final String STATISTICS_BY_SITE = "SELECT ps.ID, ps.name, SUM(ppr.Rank) AS rank"
													+ " FROM persons AS ps"
													+ " LEFT JOIN personspagerank AS ppr ON ps.ID=ppr.PersonID"
													+ " LEFT JOIN pages AS pg ON ppr.PageID=pg.ID"
													+ " LEFT JOIN sites AS s ON pg.siteID=s.ID"
													+ " WHERE s.name=?"
													+ " GROUP BY ps.ID";
	public static final String STATISTICS_BY_PERSON = "SELECT s.ID, s.name, SUM(ppr.rank) AS rank"
													+ " FROM sites AS s"
													+ " RIGHT JOIN pages AS pg ON pg.siteID=s.ID"
													+ " RIGHT JOIN personspagerank AS ppr ON ppr.PageID=pg.ID"
													+ " RIGHT JOIN persons AS ps ON ps.ID=ppr.PersonID"
													+ " WHERE ps.name=?"
													+ " GROUP BY s.ID";
	public static final String STATISTICS_BY_DATE = "SELECT pg.ID, pg.URL, ppr.Rank"
													+ " FROM sites AS s"
													+ " RIGHT JOIN pages AS pg ON pg.siteID=s.ID"
													+ " LEFT JOIN personspagerank AS ppr ON ppr.PageID=pg.ID"
													+ " LEFT JOIN persons AS ps ON ps.ID=ppr.PersonID"
													+ " WHERE s.name=?"
													+ " AND ps.name=?"
													+ " AND pg.foundDateTime BETWEEN ?"
													+ " AND ?";
    
	public DBSelect() {
		this.dbc = new DBConnect();
	}

    public ResultSet select(String query) throws SQLException {
 		ResultSet rSet = null;
 		connection = dbc.connect();
 		PreparedStatement pStatement = connection.prepareStatement(query);

		rSet = pStatement.executeQuery();
		connection.close();

 		return rSet;
 	}

    public ResultSet select(String query, String value) throws SQLException {
 		ResultSet rSet = null;
 		connection = dbc.connect();
 		PreparedStatement pStatement = connection.prepareStatement(query);

		rSet = pStatement.executeQuery();
		connection.close();

 		return rSet;
 	}
    
    public ResultSet auth(String login, String password) throws SQLException {
 		ResultSet rSet = null;
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(AUTH);

		pStatement.setString(1, login);
		pStatement.setString(2, password);
		rSet = pStatement.executeQuery();
		connection.close();

 		return rSet;
 	}

    public ResultSet statisticsByDate(String site, String person, String dFrom, String dTo)	throws SQLException {
 		ResultSet rSet = null;
 		connection = dbc.connect();
 		PreparedStatement pStatement = connection.prepareStatement(STATISTICS_BY_DATE);

 		pStatement.setString(1, site);
		pStatement.setString(2, person);
		pStatement.setString(3, dFrom);
		pStatement.setString(4, dTo);
		rSet = pStatement.executeQuery();
		connection.close();
 		
 		return rSet;
	}

//    public ResultSet sitesList() throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(SITES_LIST);
//
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}
    
//    public ResultSet pages(int i) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PAGES);
//
// 		pStatement.setInt(1, i);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//    public ResultSet pagesList() throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PAGES_LIST);
//
// 		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
// 	}

//    public ResultSet pagesCountAll(int i) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PAGES_ALL);
//
// 		pStatement.setInt(1, i);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//    public ResultSet pagesCountNotScanned(int i) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PAGES_NOT_SCANNED);
// 		
// 		pStatement.setInt(1, i);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//    public ResultSet pagesCountScanned(int i) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PAGES_SCANNED);
//
// 		pStatement.setInt(1, i);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//    public ResultSet users(String parentID) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(USERS);
//
// 		pStatement.setString(1, parentID);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//    public ResultSet usersCheckUpdate(String ID) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(USERS_CHECK_UPDATE);
//
// 		pStatement.setString(1, ID);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}
    
//    public ResultSet sites(String addedBy) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(SITES);
//
// 		pStatement.setString(1, addedBy);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//    public ResultSet sitesCheckUpdate(String ID) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(SITES_CHECK_UPDATE);
//
// 		pStatement.setString(1, ID);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
// 	}

//    public ResultSet sitesIDForPages(String siteName) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(SITES_ID_FOR_PAGES);
//
// 		pStatement.setString(1, siteName);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
// 	}

//	public ResultSet sitesForPerson(String personID) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(SITES_FOR_PERSON);
//
// 		pStatement.setString(1, personID);
//		rSet = pStatement.executeQuery();
//
// 		return rSet;
//	}

//	public ResultSet siteAllStatistics(String siteName) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(STATISTICS_BY_SITE);
//
// 		pStatement.setString(1, siteName);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
//	}

//	public ResultSet persons(String ID) throws SQLException {
//		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PERSONS);
//
// 		pStatement.setString(1, ID);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
//	}

//	public ResultSet personCheckUpdate(String personToCheck) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PERSON_CHECK_UPDATE);
//
// 		pStatement.setString(1, personToCheck);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
// 	}

//    public ResultSet personsList() throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PERSONS_LIST);
//
// 		rSet = pStatement.executeQuery();
//
// 		return rSet;
// 	}

//	public ResultSet personAllStatistics(String personName) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(STATISTICS_BY_PERSON);
//
// 		pStatement.setString(1, personName);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
//	}
    
//	public ResultSet keywordsForPerson(String personID) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(PERSON_KEYWORDS);
//
// 		pStatement.setString(1, personID);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
//	}

//	public ResultSet keywordCheckUpdate(String keywordToCheck) throws SQLException {
// 		ResultSet rSet = null;
// 		PreparedStatement pStatement = connection.prepareStatement(KEYWORD_CHECK_UPDATE);
//
// 		pStatement.setString(1, keywordToCheck);
//		rSet = pStatement.executeQuery();
// 		
// 		return rSet;
//	}
}
