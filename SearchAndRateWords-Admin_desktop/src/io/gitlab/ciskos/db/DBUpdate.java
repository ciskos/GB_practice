package io.gitlab.ciskos.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBUpdate {
	private Connection connection = null;
	private DBConnect dbc = null;
	
    public static final String UPDATE_USERS_LOGIN = "UPDATE users SET login=? WHERE id=?";
    public static final String UPDATE_USERS_EMAIL = "UPDATE users SET email=? WHERE id=?";
    public static final String UPDATE_USERS_ISADMIN = "UPDATE users SET isadmin=? WHERE id=?";
    public static final String UPDATE_USERS_PASSWORD = "UPDATE users SET password=? WHERE id=?";
    public static final String UPDATE_SITES_NAME = "UPDATE sites SET name=? WHERE id=?";
    public static final String UPDATE_SITES_DESCRIPTION = "UPDATE sites SET siteDescription=? WHERE id=?";
	public static final String UPDATE_PERSON_NAME = "UPDATE persons SET name=? WHERE id=?";
	public static final String UPDATE_KEYWORD_NAME = "UPDATE keywords SET name=? WHERE id=?";

    public DBUpdate() {
		this.dbc = new DBConnect();
	}

	public void update(String query, String id, String value) throws SQLException {
 		connection = dbc.connect();
		PreparedStatement pStatement = connection.prepareStatement(query);
		
		pStatement.setString(1, value);
		pStatement.setString(2, id);
		pStatement.executeUpdate();
		connection.close();
	}
    
/*	public void updateUsersLogin(String id, String login) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_USERS_LOGIN);
		
		pStatement.setString(1, login);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}
    
	public void updateUsersEmail(String id, String email) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_USERS_EMAIL);
		
		pStatement.setString(1, email);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}

	public void updateUsersIsAdmin(String id, String isAdmin) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_USERS_ISADMIN);
		pStatement.setString(1, isAdmin);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}

	public void updateUsersPassword(String id, String password) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_USERS_PASSWORD);
		
		pStatement.setString(1, password);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}
	
	public void updateSitesName(String id, String name) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_SITES_NAME);
		
		pStatement.setString(1, name);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}
	
	public void updateSitesDescription(String id, String description) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_SITES_DESCRIPTION);
		pStatement.setString(1, description);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}

	public void updatePersonName(String id, String name) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_PERSON_NAME);
		
		pStatement.setString(1, name);
		pStatement.setString(2, id);
		pStatement.executeUpdate(); 
	}

	public void updateKeyword(String keywordToCheck, String keywordFromTable) throws SQLException {
		PreparedStatement pStatement = connection.prepareStatement(UPDATE_KEYWORD_NAME);
		
		pStatement.setString(1, keywordFromTable);
		pStatement.setString(2, keywordToCheck);
		pStatement.executeUpdate(); 
	}*/
}
