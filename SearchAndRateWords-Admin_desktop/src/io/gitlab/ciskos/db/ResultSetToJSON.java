package io.gitlab.ciskos.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

public class ResultSetToJSON {
	public JSONArray toJSONArray(ResultSet rs) throws SQLException {
		JSONObject jo = null;
		JSONArray ja = new JSONArray();
		int columns = rs.getMetaData().getColumnCount();

		while (rs.next()) {
			StringBuffer str = new StringBuffer("{");
					
			for (int i = 1; i <= columns; i++) {
				str.append("\"" + rs.getMetaData().getColumnName(i) + "\":\"" + rs.getString(i) + "\", ");
			}
			str.delete(str.length() - 2, str.length()).append("}");
			
			String s = str.toString();
			jo = new JSONObject(s);
			ja.put(jo);
		}
			
		return ja;
	}
	
	public JSONObject toJSONObject(ResultSet rs) throws SQLException {
		JSONObject jo = null;
		int columns = rs.getMetaData().getColumnCount();
		
		while (rs.next()) {
			StringBuffer str = new StringBuffer("{");
					
			for (int i = 1; i <= columns; i++) {
				str.append("\"" + rs.getMetaData().getColumnName(i) + "\":\"" + rs.getString(i) + "\", ");
			}
			str.delete(str.length() - 2, str.length()).append("}");

			String s = str.toString();
			jo = new JSONObject(s);
		}
	
		return jo;
	}
}
