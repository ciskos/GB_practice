/**
 * 
 */
package io.gitlab.ciskos.db.tables;

/**
 * @author user
 *
 */
public class Pages extends Table {
	private int ID;
	private String URL;
	private int siteID;
	private String foundDateTime;
	private String lastScanDate;
	
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @return the uRL
	 */
	public String getURL() {
		return URL;
	}
	/**
	 * @return the siteID
	 */
	public int getSiteID() {
		return siteID;
	}
	/**
	 * @return the foundDateTime
	 */
	public String getFoundDateTime() {
		return foundDateTime;
	}
	/**
	 * @return the lastScanDate
	 */
	public String getLastScanDate() {
		return lastScanDate;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @param uRL the uRL to set
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}
	/**
	 * @param siteID the siteID to set
	 */
	public void setSiteID(int siteID) {
		this.siteID = siteID;
	}
	/**
	 * @param foundDateTime the foundDateTime to set
	 */
	public void setFoundDateTime(String foundDateTime) {
		this.foundDateTime = foundDateTime;
	}
	/**
	 * @param lastScanDate the lastScanDate to set
	 */
	public void setLastScanDate(String lastScanDate) {
		this.lastScanDate = lastScanDate;
	}
}
