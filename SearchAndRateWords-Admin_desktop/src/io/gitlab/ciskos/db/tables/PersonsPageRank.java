/**
 * 
 */
package io.gitlab.ciskos.db.tables;

/**
 * @author user
 *
 */
public class PersonsPageRank extends Table {
	private int personID;
	private int pageID;
	private int rank;
	
	/**
	 * @return the personID
	 */
	public int getPersonID() {
		return personID;
	}
	/**
	 * @return the pageID
	 */
	public int getPageID() {
		return pageID;
	}
	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}
	/**
	 * @param personID the personID to set
	 */
	public void setPersonID(int personID) {
		this.personID = personID;
	}
	/**
	 * @param pageID the pageID to set
	 */
	public void setPageID(int pageID) {
		this.pageID = pageID;
	}
	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
}
