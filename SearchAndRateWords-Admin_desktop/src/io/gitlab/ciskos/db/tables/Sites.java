/**
 * 
 */
package io.gitlab.ciskos.db.tables;

/**
 * @author user
 *
 */
public class Sites extends Table {
	private int ID;
	private String name;
	private int addedBy;
	private String siteDescription;
	
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the addedBy
	 */
	public int getAddedBy() {
		return addedBy;
	}
	/**
	 * @return the siteDescription
	 */
	public String getSiteDescription() {
		return siteDescription;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param addedBy the addedBy to set
	 */
	public void setAddedBy(int addedBy) {
		this.addedBy = addedBy;
	}
	/**
	 * @param siteDescription the siteDescription to set
	 */
	public void setSiteDescription(String siteDescription) {
		this.siteDescription = siteDescription;
	}
}
