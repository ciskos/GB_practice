/**
 * 
 */
package io.gitlab.ciskos.db.tables;

/**
 * @author user
 *
 */
public class Users extends Table {
	private int ID;
	private int parentID;
	private int isAdmin;
	private String login;
	private String password;
	private String email;
	private String token;
	private String tokenCreatedDate;
	private String tokenLastAccess;
	
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @return the parentID
	 */
	public int getParentID() {
		return parentID;
	}
	/**
	 * @return the isAdmin
	 */
	public int getIsAdmin() {
		return isAdmin;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @return the tokenCreatedDate
	 */
	public String getTokenCreatedDate() {
		return tokenCreatedDate;
	}
	/**
	 * @return the tokenLastAccess
	 */
	public String getTokenLastAccess() {
		return tokenLastAccess;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	/**
	 * @param isAdmin the isAdmin to set
	 */
	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @param tokenCreatedDate the tokenCreatedDate to set
	 */
	public void setTokenCreatedDate(String tokenCreatedDate) {
		this.tokenCreatedDate = tokenCreatedDate;
	}
	/**
	 * @param tokenLastAccess the tokenLastAccess to set
	 */
	public void setTokenLastAccess(String tokenLastAccess) {
		this.tokenLastAccess = tokenLastAccess;
	}
}
