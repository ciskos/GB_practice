package io.gitlab.ciskos.gui;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.json.JSONException;

import io.gitlab.ciskos.Main;
import io.gitlab.ciskos.db.DBSelect;

public class Authorization extends JDialog {
	private String restorePassLink = "<html><a href=\"http://www.google.com\">http://www.google.com</a></html>";
	private String loginText = "Логин";
	private String passwordText = "Пароль";
	private String enterBtnText = "Войти";
	private String errorLogOrPass = "Неверные логин или пароль, попробуйте ещё раз";
//	private String errorConnectServer = "Ошибка соединения c сервером\n";
	private String errorMsg = "Ошибка";
	private JTextField textField = new JTextField();
	private JPasswordField passwordField = new JPasswordField();
	private JPanel panel_1 = new JPanel();
	private JLabel lblNewLabel = new JLabel(loginText);
	private JLabel lblNewLabel_1 = new JLabel(passwordText);
	private JLabel lblHttpgooglecom = new JLabel(restorePassLink);
	private JButton btnNewButton = new JButton(enterBtnText);
	private GroupLayout gl_panel_1 = new GroupLayout(panel_1);
	
	/**
	 * Create the dialog.
	 */
	public Authorization() {
		setBounds(100, 100, 300, 200);
		setLocationRelativeTo(null);

		textField.setColumns(10);
		passwordField.setColumns(10);

		gl_panel_1.setAutoCreateGaps(true);
		gl_panel_1.setAutoCreateContainerGaps(true);

		panel_1.setLayout(gl_panel_1);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		getContentPane().add(panel_1);

		gl_panel_1_position();
		mouseListener();
	}

	/**
	 * 
	 */
	private void mouseListener() {
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
//					connectRest();
				try {
					connectDB();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @param gl_panel_1
	 */
	private void gl_panel_1_position() {
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap(185, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addContainerGap())
				.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
					.addGap(32)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNewLabel))
					.addGap(18)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblHttpgooglecom)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
							.addComponent(passwordField)))
					.addContainerGap(83, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(24)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(lblHttpgooglecom)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addContainerGap())
		);
	}

	/**
	 * @throws JSONException
	 * @throws HeadlessException
	 */
	private void connectDB() throws SQLException {
		DBSelect dbs = new DBSelect();
		String password = new String(passwordField.getPassword());
		String login = new String(textField.getText());
		ResultSet rs = dbs.auth(login, password);
		String idFromDB = null;
		String loginFromDB = null;
		
		while (rs.next()) {
			idFromDB = rs.getString(1);
			loginFromDB = rs.getString(2);
		}
		
		if (loginFromDB != null) {
			Main.getWindow().setVisible(true);
			Main.setUserID(idFromDB);
			Main.setUserName(loginFromDB);
			setVisible(false);
		} else {
			JOptionPane.showMessageDialog(null, errorLogOrPass, errorMsg, JOptionPane.OK_OPTION);
		}
	}
	
	/**
	 * @throws JSONException
	 * @throws HeadlessException
	 */
/*	private void connectRest() throws JSONException, HeadlessException {
		String password = new String(passwordField.getPassword());
		String login = new String(textField.getText());

		try {
			authRequest = RestRequest.authRequest(RestRequest.AUTH, login, password);
			loginRequest = new JSONObject(authRequest.toString());
			success = loginRequest.get("success").toString();
			
			if (success.equals(tru)) {
//				System.out.println("true");
				Main.setToken(loginRequest.get("token_auth").toString());
				Main.setUserID(loginRequest.get("user_id").toString());
				
//				Main.window = new MainWindow();
//				Main.isStarted = 1;
//				Main.getWindow().getFrame().setVisible(true);
				Main.getWindow().setVisible(true);
//				Main.keepConnectionAlive();
				setVisible(false);
			} else {
				JOptionPane.showMessageDialog(null, errorLogOrPass
											, errorMsg, JOptionPane.OK_OPTION);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();

			JOptionPane.showMessageDialog(null, errorConnectServer + e.getMessage()
					, errorMsg, JOptionPane.OK_OPTION);
		}
	}*/

}
