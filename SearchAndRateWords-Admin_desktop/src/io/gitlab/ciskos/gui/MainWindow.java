package io.gitlab.ciskos.gui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import io.gitlab.ciskos.gui.panels.AdminPanel;
import io.gitlab.ciskos.gui.panels.LogPanel;
import io.gitlab.ciskos.gui.panels.UserPanel;
import io.gitlab.ciskos.gui.panels.logTabs.AdminToUserBtn;

public class MainWindow extends JFrame {

	private String a2u = "<html><center>Перейти в режим<br>пользователя</center></html>";
	private AdminToUserBtn adm2usr;
	private JPanel aPanel;
	private JPanel uPanel;
	private JPanel lPanel;
	private GroupLayout groupLayout;

	/**
	 * Create the application.
	 */
	public MainWindow() {
//		TopMenu menuBar = new TopMenu();
		groupLayout = new GroupLayout(this.getContentPane());
		aPanel = new AdminPanel(this);
		uPanel = new UserPanel();
		adm2usr = new AdminToUserBtn(a2u, this);
		lPanel = new LogPanel(this);

		setSize(1024, 768);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setJMenuBar(menuBar);
		getContentPane().setLayout(groupLayout);

		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);

		groupLayout_position();
		pack();
	}

	/**
	 * 
	 */
	private void groupLayout_position() {
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(aPanel, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lPanel, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
		);
		
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addComponent(aPanel, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lPanel, 0, GroupLayout.DEFAULT_SIZE, 200)
					.addContainerGap())
		);
	}

	/**
	 * @return the aPanel
	 */
	public AdminPanel getaPanel() {
		return (AdminPanel)aPanel;
	}

	/**
	 * @return the uPanel
	 */
	public UserPanel getuPanel() {
		return (UserPanel)uPanel;
	}

	/**
	 * @return the lPanel
	 */
	public LogPanel getlPanel() {
		return (LogPanel)lPanel;
	}

	/**
	 * @return the groupLayout
	 */
	public GroupLayout getGroupLayout() {
		return groupLayout;
	}

	/**
	 * @return the adm2usr
	 */
	public AdminToUserBtn getAdm2usr() {
		return adm2usr;
	}
}
