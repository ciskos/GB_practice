package io.gitlab.ciskos.gui.panels;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import io.gitlab.ciskos.gui.MainWindow;
import io.gitlab.ciskos.gui.panels.adminTabs.PersonsTab;
import io.gitlab.ciskos.gui.panels.adminTabs.SitesStatisticsTab;
import io.gitlab.ciskos.gui.panels.adminTabs.SitesTab;
import io.gitlab.ciskos.gui.panels.adminTabs.UsersTab;

public class AdminPanel extends JPanel {

	private MainWindow mainWindow;
	private GroupLayout gl_panel;
	private JTabbedPane tabbedPane;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_5;



	/**
	 * @param mainWindow 
	 * @param mainWindow 
	 * @param logPanel 
	 * 
	 */
	public AdminPanel(MainWindow mainWindow) {
		this.setBackground(Color.LIGHT_GRAY);
		this.mainWindow = mainWindow;
		this.setLayout(gl_panel);

		gl_panel = new GroupLayout(this);
		tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		panel_2 = new SitesStatisticsTab();
		panel_3 = new UsersTab();
		panel_4 = new SitesTab();
		panel_5 = new PersonsTab();
		
		tabbedPane.addTab("Статистика обхода сайтов", null, panel_2, null);
		tabbedPane.addTab("Пользователи", null, panel_3, null);
		tabbedPane.addTab("Сайты", null, panel_4, null);
		tabbedPane.addTab("Персоны", null, panel_5, null);
		
		gl_panel.setAutoCreateGaps(true);
		gl_panel.setAutoCreateContainerGaps(true);
		
		gl_panel_position();
	}

	/**
	 * 
	 */
	private void gl_panel_position() {
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(tabbedPane, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(tabbedPane, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
	}

}
