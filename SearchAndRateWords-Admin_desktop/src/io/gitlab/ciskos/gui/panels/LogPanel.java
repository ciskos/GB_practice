package io.gitlab.ciskos.gui.panels;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;

import io.gitlab.ciskos.gui.MainWindow;
import io.gitlab.ciskos.gui.panels.logTabs.LogTable;

public class LogPanel extends JPanel {
	
	private MainWindow mWindow;
	private JTable table;
	private GroupLayout gl_panel_1;
	private JScrollPane scrollPane;
	private JButton btnNewButton;
	
	/**
	 * @param admin2user 
	 * 
	 */
	public LogPanel(MainWindow mWindow) {
		this.mWindow = mWindow;
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(gl_panel_1);

		gl_panel_1 = new GroupLayout(this);
		scrollPane = new JScrollPane();
		table = new LogTable();
		btnNewButton = mWindow.getAdm2usr();
		
		scrollPane.setViewportView(table);
		
		gl_panel_1.setAutoCreateGaps(true);
		gl_panel_1.setAutoCreateContainerGaps(true);
		
		gl_panel_1_position();
	}

	/**
	 * 
	 */
	private void gl_panel_1_position() {
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addComponent(btnNewButton, 0, GroupLayout.DEFAULT_SIZE, 150)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(scrollPane, 0, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE))
				);
		
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, Alignment.TRAILING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addComponent(btnNewButton, 0, GroupLayout.DEFAULT_SIZE, 40))
				);
	}

	/**
	 * @return the table
	 */
	public JTable getTable() {
		return table;
	}
}
