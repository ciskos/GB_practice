package io.gitlab.ciskos.gui.panels;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import io.gitlab.ciskos.gui.panels.userTabs.DailyStatistics;
import io.gitlab.ciskos.gui.panels.userTabs.TotalStatistics;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class UserPanel extends JPanel {

	private GroupLayout gl_panel;
	private JTabbedPane tabbedPane;
	private JPanel panel_2;
	private JPanel panel_3;

	/**
	 * 
	 */
	public UserPanel() {
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(gl_panel);

		gl_panel = new GroupLayout(this);
		tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		panel_2 = new TotalStatistics();
		panel_3 = new DailyStatistics();

		tabbedPane.addTab("Общая статистика", null, panel_2, null);
		tabbedPane.addTab("Ежедневная статисика", null, panel_3, null);

		gl_panel.setAutoCreateGaps(true);
		gl_panel.setAutoCreateContainerGaps(true);

		gl_panel_position();
	}

	/**
	 * 
	 */
	private void gl_panel_position() {
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(tabbedPane, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(tabbedPane, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
	}

}
