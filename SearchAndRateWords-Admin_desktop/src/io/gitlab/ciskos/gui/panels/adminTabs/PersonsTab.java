package io.gitlab.ciskos.gui.panels.adminTabs;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.gui.panels.adminTabs.tabElements.PersonsTabListeners;

public class PersonsTab extends JPanel {
	private GroupLayout groupLayout = new GroupLayout(this);
	private JTextField textField = new JTextField();
	private JTable table = new JTable();
	private JTable table_1 = new JTable();
	private JTable table_2 = new JTable();
	private JScrollPane scrollPane = new JScrollPane();
	private JScrollPane scrollPane_1 = new JScrollPane();
	private JScrollPane scrollPane_2 = new JScrollPane();
	private JButton button = new JButton("Обновить");
	private JButton button_1 = new JButton("Сохранить изменения");
	private JButton button_2 = new JButton("Удалить");
	private JButton button_3 = new JButton("Добавить");
	private JButton button_4 = new JButton("Сохранить изменения");
	private JButton button_5 = new JButton("Удалить");
	private JButton button_6 = new JButton("Обновить");
	private JButton button_7 = new JButton("Добавить");
	private JButton btnCsv = new JButton("Выгрузить в CSV");
	private JLabel label = new JLabel("Добавить персону");
	private JLabel label_1 = new JLabel("Ключевые слова");
	private JLabel lblNewLabel = new JLabel("Используется на сайтах");
	private JSeparator separator = new JSeparator();
	private String[] tableHeader = new String[] {"№", "Персона"};
	private String[] table_1Header = new String[] {"№", "Ключевое слово"};
	private String[] table_2Header = new String[] {"№", "Сайт"};
	private Object[][] tableObject = new Object[][] {{null, null},};
	private Object[][] table_1Object = new Object[][] {{null, null},};
	private Object[][] table_2Object = new Object[][] {{null, null},};
	private DefaultTableModel tableDTM = new DefaultTableModel(tableObject, tableHeader);
	private DefaultTableModel table_1DTM = new DefaultTableModel(table_1Object, table_1Header);
	private DefaultTableModel table_2DTM = new DefaultTableModel(table_2Object, table_2Header);

	/**
	 *
	 */
	public PersonsTab() {
//		groupLayout = new GroupLayout(this);
		PersonsTabListeners ptl = new PersonsTabListeners(this);
		
		setLayout(groupLayout);
		textField.setColumns(10);
		table.setModel(tableDTM);
		table_1.setModel(table_1DTM);
		table_2.setModel(table_2DTM);
		scrollPane.setViewportView(table);
		scrollPane_1.setViewportView(table_1);
		scrollPane_2.setViewportView(table_2);

		ptl.button_AL();
		ptl.button_1_AL();
		ptl.button_2_AL();
		ptl.button_3_AL();
		ptl.button_4_AL();
		ptl.button_5_AL();
		ptl.button_6_AL();
		ptl.button_7_AL();
		LittleMethods.btnCsv_AL(btnCsv, table);
		ptl.table_ML();

		groupLayout_position();
	}

	/**
	 * 
	 */
	private void groupLayout_position() {
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(separator, GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(label)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_3))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(button_6, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(button_4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(button_5, GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
												.addComponent(button_7, GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)))
										.addComponent(label_1, Alignment.LEADING)
										.addComponent(scrollPane_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE))
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel)
										.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 243, GroupLayout.PREFERRED_SIZE)))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(button)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_1)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_2)
									.addPreferredGap(ComponentPlacement.RELATED, 165, Short.MAX_VALUE)
									.addComponent(btnCsv)))
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(button)
						.addComponent(button_1)
						.addComponent(button_2)
						.addComponent(btnCsv))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(button_3))
					.addGap(8)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(lblNewLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(button_6)
								.addComponent(button_7))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(button_4)
								.addComponent(button_5)))
						.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE))
					.addContainerGap())
		);
	}

	/**
	 * @return the table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * @return the table_1
	 */
	public JTable getTable_1() {
		return table_1;
	}

	/**
	 * @return the table_2
	 */
	public JTable getTable_2() {
		return table_2;
	}

	/**
	 * @return the tableHeader
	 */
	public String[] getTableHeader() {
		return tableHeader;
	}

	/**
	 * @return the table_1Header
	 */
	public String[] getTable_1Header() {
		return table_1Header;
	}

	/**
	 * @return the table_2Header
	 */
	public String[] getTable_2Header() {
		return table_2Header;
	}
	/**
	 * @return the tableDTM
	 */
	public DefaultTableModel getTableDTM() {
		return tableDTM;
	}

	/**
	 * @return the table_1DTM
	 */
	public DefaultTableModel getTable_1DTM() {
		return table_1DTM;
	}

	/**
	 * @return the table_2DTM
	 */
	public DefaultTableModel getTable_2DTM() {
		return table_2DTM;
	}

	/**
	 * @return the textField
	 */
	public JTextField getTextField() {
		return textField;
	}

	/**
	 * @return the button
	 */
	public JButton getButton() {
		return button;
	}

	/**
	 * @return the button_1
	 */
	public JButton getButton_1() {
		return button_1;
	}

	/**
	 * @return the button_2
	 */
	public JButton getButton_2() {
		return button_2;
	}

	/**
	 * @return the button_3
	 */
	public JButton getButton_3() {
		return button_3;
	}

	/**
	 * @return the button_4
	 */
	public JButton getButton_4() {
		return button_4;
	}

	/**
	 * @return the button_5
	 */
	public JButton getButton_5() {
		return button_5;
	}

	/**
	 * @return the button_6
	 */
	public JButton getButton_6() {
		return button_6;
	}

	/**
	 * @return the button_7
	 */
	public JButton getButton_7() {
		return button_7;
	}
}
