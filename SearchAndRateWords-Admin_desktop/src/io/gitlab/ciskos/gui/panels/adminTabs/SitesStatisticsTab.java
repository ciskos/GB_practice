/**
 * 
 */
package io.gitlab.ciskos.gui.panels.adminTabs;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.gui.panels.adminTabs.tabElements.SiteStatisticsTabListeners;

/**
 * @author user
 *
 */
public class SitesStatisticsTab extends JPanel {
	private GroupLayout gl_panel_2 = new GroupLayout(this);
//	private JComboBox<String> comboBox = null;
	private JComboBox<String> comboBox_3 = new JComboBox<>();
	private JLabel lblLentaru = new JLabel();
	private JButton button = new JButton("Применить");
	private JButton btnCsv = new JButton("Выгрузить в CSV");
	private JScrollPane scrollPane_1 = new JScrollPane();
	private JScrollPane scrollPane_2 = new JScrollPane();
	private JTable table_1 = new JTable();
	private JTable table_2 = new JTable();
	private Object[][] table_1Object = new Object[][] {{null, null, null, null, null}};
	private Object[][] table_2Object = new Object[][] {
												{"Находятся в базе данных", null},
												{"никогда не обходились, но находятся в базе данных", null},
												{"Уже обходились", null},
											};
	private String[] table_1Header = {"ID", "URL", "SiteID", "FoundDateTime", "LastScanDate"};
	private String[] table_2Header = new String[] {"Ссылки, которые:", "Количество"};
	private DefaultTableModel table_1DTM = new DefaultTableModel(table_1Object, table_1Header);
	private DefaultTableModel table_2DTM = new DefaultTableModel(table_2Object, table_2Header);
	
	/**
	 * 
	 */
	public SitesStatisticsTab() {
		setLayout(gl_panel_2);
		SiteStatisticsTabListeners sstl = new SiteStatisticsTabListeners(this);
		
		DBSelect dbs = new DBSelect();
		ResultSet rs = null;
		try {
			rs = dbs.select(DBSelect.SITES_LIST);
			comboBox_3 = LittleMethods.rsToCB(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		table_1.setModel(table_1DTM);
		table_2.setModel(table_2DTM);
		table_2.getColumnModel().getColumn(0).setPreferredWidth(285);

		scrollPane_1.setViewportView(table_1);
		scrollPane_2.setViewportView(table_2);
		
		lblLentaru.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		sstl.button_AL();
		LittleMethods.btnCsv_AL(btnCsv, table_1);
		
		gl_panel_2_position();
	}

	/**
	 * 
	 */
	private void gl_panel_2_position() {
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(comboBox_3, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(button)
							.addPreferredGap(ComponentPlacement.RELATED, 182, Short.MAX_VALUE)
							.addComponent(lblLentaru, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
						.addComponent(btnCsv))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(button)
						.addComponent(lblLentaru))
					.addGap(18)
					.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCsv)
					.addGap(55)
					.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
					.addContainerGap())
		);
	}

	/**
	 * @return the comboBox
	 */
	public JComboBox<String> getComboBox_3() {
		return comboBox_3;
	}

	/**
	 * @return the lblLentaru
	 */
	public JLabel getLblLentaru() {
		return lblLentaru;
	}

	/**
	 * @return the button
	 */
	public JButton getButton() {
		return button;
	}


	/**
	 * @return the table_1
	 */
	public JTable getTable_1() {
		return table_1;
	}

	/**
	 * @return the table_2
	 */
	public JTable getTable_2() {
		return table_2;
	}


	/**
	 * @return the table_2Object
	 */
	public Object[][] getTable_2Object() {
		return table_2Object;
	}

	/**
	 * @return the table_1Header
	 */
	public String[] getTable_1Header() {
		return table_1Header;
	}

	/**
	 * @return the table_2Header
	 */
	public String[] getTable_2Header() {
		return table_2Header;
	}

	/**
	 * @return the table_1DTM
	 */
	public DefaultTableModel getTable_1DTM() {
		return table_1DTM;
	}

	/**
	 * @return the table_2DTM
	 */
	public DefaultTableModel getTable_2DTM() {
		return table_2DTM;
	}
}
