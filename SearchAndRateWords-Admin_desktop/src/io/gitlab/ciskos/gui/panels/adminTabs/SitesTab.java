package io.gitlab.ciskos.gui.panels.adminTabs;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.gui.panels.adminTabs.tabElements.SitesTabListeners;

public class SitesTab extends JPanel {
	private GroupLayout groupLayout = new GroupLayout(this);
	private JTextField textField = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JScrollPane scrollPane = new JScrollPane();
	private JButton button = new JButton("Обновить");
	private JButton button_1 = new JButton("Сохранить изменения");
	private JButton button_2 = new JButton("Удалить");
	private JButton button_3 = new JButton("Сохранить");
	private JButton btnCsv = new JButton("Выгрузить в CSV");
	private JLabel label = new JLabel("Добавить сайт");
	private JLabel label_1 = new JLabel("Имя сайта");
	private JLabel label_2 = new JLabel("Описание сайта");
	private JLabel lblUrl = new JLabel("URL");
	private JTextArea textArea = new JTextArea();
	private JTable table = new JTable();
	private Object[][] tableObject = new Object[][] {{null, null, null},};
	private String[] tableHeader = new String[] {"№", "Имя сайта", "Описание сайта"};
	private DefaultTableModel tableDTM = new DefaultTableModel(tableObject,tableHeader);

	/**
	 * Create the panel.
	 */
	public SitesTab() {
		SitesTabListeners stl = new SitesTabListeners(this);
		
		textField.setColumns(10);
		textField_1.setColumns(30);
		
		table.setModel(tableDTM);
		table.getColumnModel().getColumn(2).setPreferredWidth(112);
		scrollPane.setViewportView(table);
		setLayout(groupLayout);

		stl.button_AL();
		stl.button_1_AL();
		stl.button_2_AL();
		stl.button_3_AL();
		LittleMethods.btnCsv_AL(btnCsv, table);

		groupLayout_position();
	}

	/**
	 * 
	 */
	private void groupLayout_position() {
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(textArea, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
						.addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(button)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(button_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(button_2)
							.addPreferredGap(ComponentPlacement.RELATED, 174, Short.MAX_VALUE)
							.addComponent(btnCsv))
						.addComponent(label, Alignment.LEADING)
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addComponent(label_1)
							.addGap(18)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lblUrl)
							.addGap(18)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(label_2, Alignment.LEADING)
						.addComponent(button_3))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(button)
						.addComponent(button_1)
						.addComponent(button_2)
						.addComponent(btnCsv))
					.addGap(18)
					.addComponent(label)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblUrl)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(label_2)
					.addGap(18)
					.addComponent(textArea, GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(button_3)
					.addContainerGap())
		);
	}

	/**
	 * @return the textField
	 */
	public JTextField getTextField() {
		return textField;
	}

	/**
	 * @return the textField_1
	 */
	public JTextField getTextField_1() {
		return textField_1;
	}

	/**
	 * @return the button
	 */
	public JButton getButton() {
		return button;
	}

	/**
	 * @return the button_1
	 */
	public JButton getButton_1() {
		return button_1;
	}

	/**
	 * @return the button_2
	 */
	public JButton getButton_2() {
		return button_2;
	}

	/**
	 * @return the button_3
	 */
	public JButton getButton_3() {
		return button_3;
	}

	/**
	 * @return the btnCsv
	 */
	public JButton getBtnCsv() {
		return btnCsv;
	}

	/**
	 * @return the textArea
	 */
	public JTextArea getTextArea() {
		return textArea;
	}

	/**
	 * @return the table
	 */
	public JTable getTable() {
		return table;
	}


	/**
	 * @return the tableHeader
	 */
	public String[] getTableHeader() {
		return tableHeader;
	}

	/**
	 * @return the tableDTM
	 */
	public DefaultTableModel getTableDTM() {
		return tableDTM;
	}
}
