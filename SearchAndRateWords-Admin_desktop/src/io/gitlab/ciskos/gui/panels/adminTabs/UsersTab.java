package io.gitlab.ciskos.gui.panels.adminTabs;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.gui.panels.adminTabs.tabElements.UsersTabListeners;

public class UsersTab extends JPanel {
	private JTextField textField = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JTextField textField_2 = new JTextField();
	private JTextField textField_4 = new JTextField();
//	private StringBuffer ur;
	private JScrollPane scrollPane = new JScrollPane();
	private JButton button = new JButton("Обновить");
	private JButton button_1 = new JButton("Сохранить изменения");
	private JButton button_2 = new JButton("Удалить");
	private JButton button_3 = new JButton("Сохранить");
	private JButton button_4 = new JButton("Сохранить");
	private JButton btnCsv = new JButton("Выгрузить в CSV");
	private JPanel panel = new JPanel();
	private JPanel panel_1 = new JPanel();
	private JSeparator separator = new JSeparator();
	private GroupLayout groupLayout = new GroupLayout(this);
	private GroupLayout gl_panel = new GroupLayout(panel);
	private GroupLayout gl_panel_1 = new GroupLayout(panel_1);
	private JLabel label = new JLabel("Логин");
	private JLabel label_1 = new JLabel("Пароль");
	private JLabel label_2 = new JLabel("E-mail");
	private JLabel label_3 = new JLabel("Добавить нового пользователя");
	private JLabel label_5 = new JLabel("Новый пароль");
	private JLabel lblNewLabel = new JLabel("Изменить пароль");
	private JCheckBox checkBox = new JCheckBox("Сделать админом?");
	private JTable table = new JTable();
	private Object[][] tableObject = new Object[][] {{"", "", "", null},};
	private String[] tableHeader = {"№", "Пользователь", "E-mail", "Является администратором"};
	private DefaultTableModel tableDTM = new DefaultTableModel(tableObject,tableHeader);

	/**
	 * Create the panel.
	 */
	public UsersTab() {
		UsersTabListeners utl = new UsersTabListeners(this);
		
		setLayout(groupLayout);
		
		textField.setColumns(10);
		textField_1.setColumns(10);
		textField_2.setColumns(10);
		textField_4.setColumns(10);
		
		panel.setLayout(gl_panel);
		panel_1.setLayout(gl_panel_1);
		
		table.setModel(tableDTM);
		scrollPane.setViewportView(table);

		utl.button_AL();
		utl.button_1_AL();
		utl.button_2_AL();
		utl.button_3_AL();
		utl.button_4_AL();
		LittleMethods.btnCsv_AL(btnCsv, table);

		groupLayout_position();
		gl_panel_position();
		gl_panel_1_position();
	}

	/**
	 * 
	 */
	private void gl_panel_1_position() {
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(label_5)
							.addGap(18)
							.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
							.addComponent(button_4)))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_5)
						.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(21, Short.MAX_VALUE))
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap(43, Short.MAX_VALUE)
					.addComponent(button_4))
		);
	}

	/**
	 * 
	 */
	private void gl_panel_position() {
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(label)
							.addGap(18)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(label_1)
							.addGap(18)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(label_2)
							.addGap(18)
							.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(checkBox))
						.addComponent(label_3))
					.addContainerGap(20, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(523, Short.MAX_VALUE)
					.addComponent(button_3))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(label_3)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_2)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(checkBox))
					.addPreferredGap(ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
					.addComponent(button_3))
		);
	}

	/**
	 * 
	 */
	private void groupLayout_position() {
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 622, Short.MAX_VALUE)
						.addComponent(separator, GroupLayout.PREFERRED_SIZE, 622, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(button)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_1)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_2)
									.addPreferredGap(ComponentPlacement.RELATED, 212, Short.MAX_VALUE)
									.addComponent(btnCsv)))
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(button_1)
						.addComponent(button_2)
						.addComponent(btnCsv)
						.addComponent(button))
					.addGap(26)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator, GroupLayout.DEFAULT_SIZE, 4, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
					.addContainerGap())
		);
	}

	/**
	 * @return the textField
	 */
	public JTextField getTextField() {
		return textField;
	}

	/**
	 * @return the textField_1
	 */
	public JTextField getTextField_1() {
		return textField_1;
	}

	/**
	 * @return the textField_2
	 */
	public JTextField getTextField_2() {
		return textField_2;
	}

	/**
	 * @return the textField_4
	 */
	public JTextField getTextField_4() {
		return textField_4;
	}

	/**
	 * @return the button
	 */
	public JButton getButton() {
		return button;
	}

	/**
	 * @return the button_1
	 */
	public JButton getButton_1() {
		return button_1;
	}

	/**
	 * @return the button_2
	 */
	public JButton getButton_2() {
		return button_2;
	}

	/**
	 * @return the button_3
	 */
	public JButton getButton_3() {
		return button_3;
	}

	/**
	 * @return the button_4
	 */
	public JButton getButton_4() {
		return button_4;
	}

	/**
	 * @return the checkBox
	 */
	public JCheckBox getCheckBox() {
		return checkBox;
	}

	/**
	 * @return the table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * @return the tableHeader
	 */
	public String[] getTableHeader() {
		return tableHeader;
	}

	/**
	 * @return the tableDTM
	 */
	public DefaultTableModel getTableDTM() {
		return tableDTM;
	}
}
