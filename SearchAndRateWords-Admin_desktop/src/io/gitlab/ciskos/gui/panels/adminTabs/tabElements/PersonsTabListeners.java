package io.gitlab.ciskos.gui.panels.adminTabs.tabElements;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.Main;
import io.gitlab.ciskos.db.DBDelete;
import io.gitlab.ciskos.db.DBInsert;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.db.DBUpdate;
import io.gitlab.ciskos.gui.panels.adminTabs.PersonsTab;

public class PersonsTabListeners {

	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private String[] tableHeader;
	private String[] table_1Header;
	private String[] table_2Header;
	private DefaultTableModel tableDTM;
	private DefaultTableModel table_1DTM;
	private DefaultTableModel table_2DTM;
	private JTextField textField;
	private JButton button;
	private JButton button_1;
	private JButton button_2;
	private JButton button_3;
	private JButton button_4;
	private JButton button_5;
	private JButton button_6;
	private JButton button_7;
	
	public PersonsTabListeners(PersonsTab pt) {
		this.table = pt.getTable();
		this.table_1 = pt.getTable_1();
		this.table_2 = pt.getTable_2();
		this.tableHeader = pt.getTableHeader();
		this.table_1Header = pt.getTable_1Header();
		this.table_2Header = pt.getTable_2Header();
		this.tableDTM = pt.getTableDTM();
		this.table_1DTM = pt.getTable_1DTM();
		this.table_2DTM = pt.getTable_2DTM();
		this.textField = pt.getTextField();
		this.button = pt.getButton();
		this.button_1 = pt.getButton_1();
		this.button_2 = pt.getButton_2();
		this.button_3 = pt.getButton_3();
		this.button_4 = pt.getButton_4();
		this.button_5 = pt.getButton_5();
		this.button_6 = pt.getButton_6();
		this.button_7 = pt.getButton_7();
	}

	/**
	 * 
	 */
	public void table_ML() {
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (table.getValueAt(0, 0) != null) {
					String person = table.getValueAt(table.getSelectedRow(), 0).toString();
					DBSelect dbs = new DBSelect();
					ResultSet rsSites = null;
					ResultSet rsKeywords = null;

					try {
						rsSites = dbs.select(DBSelect.SITES_FOR_PERSON, person);
						rsKeywords = dbs.select(DBSelect.PERSON_KEYWORDS, person);
						if (rsSites != null) {
							table_2DTM = LittleMethods.rsToDTM(rsSites, table_2Header);
							table_2.setModel(table_2DTM);
							table_2.setRowSorter(new TableRowSorter<DefaultTableModel>(table_2DTM));
						}
						if (rsKeywords != null) {
							table_1DTM = LittleMethods.rsToDTM(rsKeywords, table_1Header);
							table_1.setModel(table_1DTM);
							table_1.setRowSorter(new TableRowSorter<DefaultTableModel>(table_1DTM));
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}
	
	public void button_7_AL() {
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String keyword = JOptionPane.showInputDialog(null, "Введите ключевое слово", "Ввод данных",
						JOptionPane.OK_CANCEL_OPTION);

				try {
					if (keyword != null & table.getValueAt(0, 0) != null & table.getSelectedRow() > 0) {
						String personID = table.getValueAt(table.getSelectedRow(), 0).toString();
						String keywordAdded = Main.getUserName() + " добавил ключевое слово " + keyword
								+ " в таблицу \"keywords\" ";
						DBInsert dbi = new DBInsert();
						DBSelect dbs = new DBSelect();
						ResultSet rsKeywordsList = dbs.select(DBSelect.PERSON_KEYWORDS, personID);
						boolean personAlreadyInBase = false;

						while (rsKeywordsList.next()) {
							if (rsKeywordsList.getString("name").equals(keyword)) {
								personAlreadyInBase = true;
								break;
							}
						}

						if (!personAlreadyInBase) {
							if (keyword.length() > 0) {
								JTable tlp = Main.getWindow().getlPanel().getTable();
								DefaultTableModel model = (DefaultTableModel) tlp.getModel();
								
								model.addRow(new Object[] { tlp.getRowCount() + 1, keywordAdded });

								dbi.insertKeyword(personID, keyword);
								dbi.logAdd(keywordAdded, Main.getUserID());
							} else {
								JOptionPane.showMessageDialog(null, "Введите ключевое слово", "Ошибка",
										JOptionPane.OK_OPTION);
							}
						} else {
							JOptionPane.showMessageDialog(null, "Такое слово уже есть", "Ошибка",
									JOptionPane.OK_OPTION);
						}
					}
				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_6_AL() {
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table.getValueAt(0, 0) != null & table.getSelectedRow() > 0) {
					String person = table.getValueAt(table.getSelectedRow(), 0).toString();
					DBSelect dbs = new DBSelect();
					ResultSet rs = null;

					try {
						rs = dbs.select(DBSelect.PERSON_KEYWORDS, person);
						if (rs != null) {
							table_1DTM = LittleMethods.rsToDTM(rs, tableHeader);
							table_1.setModel(table_1DTM);
							table_1.setRowSorter(new TableRowSorter<DefaultTableModel>(table_1DTM));
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_5_AL() {
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedRow() > -1 & table_1.getValueAt(0, 0) != null) {
					int dialogResult = JOptionPane.showConfirmDialog(null, "Действительно удалить?", "Вопрос",
							JOptionPane.YES_NO_OPTION);
					if (dialogResult == JOptionPane.YES_OPTION) {
						DBDelete dbd = new DBDelete();
						DBInsert dbi = new DBInsert();
						String keywordDeleted = Main.getUserName() + " удалил ключевое слово "
								+ table_1.getValueAt(table_1.getSelectedRow(), 1).toString()
								+ " из таблицы \"persons\" "; 
						String keywordToDelete = table_1.getValueAt(table_1.getSelectedRow(), 0).toString();
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();

						model.addRow(new Object[]{tlp.getRowCount() + 1, keywordDeleted});

						try {
							dbd.delete(DBDelete.DELETE_KEYWORD, keywordToDelete);
							dbi.logAdd(keywordDeleted, Main.getUserID());
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_4_AL() {
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedRow() > -1) {
					String keywordFromDB = null;
					String keywordFromTable = table_1.getValueAt(table_1.getSelectedRow(), 1).toString();
					String keywordModified = Main.getUserName() + " изменил ключевое слово с " + keywordFromDB + " на "
							+ keywordFromTable + " в таблице \"persons\" ";
					String keywordToCheck = table_1.getValueAt(table_1.getSelectedRow(), 0).toString();
					DBSelect dbs = new DBSelect();
					DBUpdate dbu = new DBUpdate();
					DBInsert dbi = new DBInsert();
					ResultSet rs = null;

					try {
						rs = dbs.select(DBSelect.KEYWORD_CHECK_UPDATE, keywordToCheck);
						rs.next();
						keywordFromDB = rs.getString(2).toString();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

					// TODO нужна проверка вводимых данных
					if (!keywordFromDB.equals(keywordFromTable)) {
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();

						model.addRow(new Object[] { tlp.getRowCount() + 1, keywordModified });

						try {
							dbu.update(DBUpdate.UPDATE_KEYWORD_NAME, keywordToCheck, keywordFromTable);
							dbi.logAdd(keywordModified, Main.getUserID());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_3_AL() {
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean personAlreadyInBase = false;
				String personName = textField.getText();
				String personAdded = Main.getUserName() + " добавил персону "
						+ personName + " в таблицу \"persons\" ";
				DBInsert dbi = new DBInsert();
				DBSelect dbs = new DBSelect();
				ResultSet rsPersonList = null;
				
				try {
					rsPersonList = dbs.select(DBSelect.PERSONS, Main.getUserID());

					while (rsPersonList.next()) {
						if (rsPersonList.getString("name").equals(personName)) {
							personAlreadyInBase = true;
							break;
						}
					}
				} catch (SQLException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}

				if (!personAlreadyInBase) {
					if (personName.length() > 0) {
//						ResultSetToJSON rsToJSON = new ResultSetToJSON();
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();
				
						model.addRow(new Object[]{tlp.getRowCount() + 1, personAdded});
						textField.setText("");

						try {
							dbi.insertPerson(personName, Main.getUserID());
							dbi.logAdd(personAdded, Main.getUserID());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					} else {
						JOptionPane.showMessageDialog(null, "Заполните обязательное поле - Добавить персону",
								"Ошибка", JOptionPane.OK_OPTION);
					} 
				} else {
					JOptionPane.showMessageDialog(null, "Такая персона уже есть",
							"Ошибка", JOptionPane.OK_OPTION);
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_2_AL() {
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				if (table.getSelectedRow() >= 0 && table.getValueAt(0, 0) != "") {
				if (table.getValueAt(0, 0) != null) {
					int dialogResult = JOptionPane.showConfirmDialog(null, "Действительно удалить?", "Вопрос",
							JOptionPane.YES_NO_OPTION);
					if (dialogResult == JOptionPane.YES_OPTION) {
						DBDelete dbd = new DBDelete();
						DBInsert dbi = new DBInsert();
						String personDeleted = Main.getUserName() + " удалил персону "
											+ table.getValueAt(table.getSelectedRow(), 1).toString()
											+ " из таблицы \"persons\" "; 
						String personToDelete = table.getValueAt(table.getSelectedRow(), 0).toString();
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();

						model.addRow(new Object[]{tlp.getRowCount() + 1, personDeleted});

						try {
							dbd.delete(DBDelete.DELETE_PERSONSPAGERANK_PERSONS, personToDelete);
							dbd.delete(DBDelete.DELETE_KEYWORDS, personToDelete);
							dbd.delete(DBDelete.DELETE_PERSON, personToDelete);
							dbi.logAdd(personDeleted, Main.getUserID());
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_1_AL() {
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() >= 0) {
					String nameFromDB = null;
					String nameFromTable = table.getValueAt(table.getSelectedRow(), 1).toString();
					String personModified = Main.getUserName() + " изменил персону "
							+ nameFromDB + " в таблицe \"persons\" "
							+ " на " + nameFromTable; 
					String personToCheck = table.getValueAt(table.getSelectedRow(), 0).toString();
					DBSelect dbs = new DBSelect();
					DBUpdate dbu = new DBUpdate();
					DBInsert dbi = new DBInsert();
					ResultSet rs = null;
					
					try {
						rs = dbs.select(DBSelect.PERSON_CHECK_UPDATE, personToCheck);
						rs.next();
						nameFromDB = rs.getString(2).toString();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
					
					// TODO нужна проверка вводимых данных
					if (!nameFromDB.equals(nameFromTable)) {
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();

						model.addRow(new Object[]{tlp.getRowCount() + 1, personModified});
						
						try {
							dbu.update(DBUpdate.UPDATE_PERSON_NAME, personToCheck, nameFromTable);
							dbi.logAdd(personModified, Main.getUserID());
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_AL() {
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DBSelect dbs = new DBSelect();
				ResultSet rs = null;
				
				try {
					rs = dbs.select(DBSelect.PERSONS, Main.getUserID());
					if (rs != null) {
						tableDTM = LittleMethods.rsToDTM(rs, tableHeader);
						table.setModel(tableDTM);
						table.setRowSorter(new TableRowSorter<DefaultTableModel>(tableDTM));
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});
	}
}
