/**
 * 
 */
package io.gitlab.ciskos.gui.panels.adminTabs.tabElements;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.gui.panels.adminTabs.SitesStatisticsTab;

/**
 * @author user
 *
 */
public class SiteStatisticsTabListeners {
	
	private JComboBox<String> comboBox;
	private JLabel lblLentaru;
	private JButton button;
	private JTable table_1;
	private JTable table_2;
	private Object[][] table_2Object;
	private String[] table_1Header;
	private String[] table_2Header;
	private DefaultTableModel table_1DTM;
	private DefaultTableModel table_2DTM;
	
	public SiteStatisticsTabListeners(SitesStatisticsTab sst) {
		this.comboBox = sst.getComboBox_3();
		this.lblLentaru = sst.getLblLentaru();
		this.button = sst.getButton();
		this.table_1 = sst.getTable_1();
		this.table_2 = sst.getTable_2();
		this.table_2Object = sst.getTable_2Object();
		this.table_1Header = sst.getTable_1Header();
		this.table_2Header = sst.getTable_2Header();
		this.table_1DTM = sst.getTable_1DTM();
		this.table_2DTM = sst.getTable_2DTM();
	}

	/**
	 * 
	 */
	public void button_AL() {
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DBSelect dbs = new DBSelect();
				ResultSet rs = null;
				ResultSet rsAll = null;
				ResultSet rsScanned = null;
				ResultSet rsNotScanned = null;
				String siteID = String.valueOf(comboBox.getSelectedIndex() + 1);
				
				try {
					rs = dbs.select(DBSelect.PAGES, siteID);
					rsAll = dbs.select(DBSelect.PAGES_ALL, siteID);
					rsScanned = dbs.select(DBSelect.PAGES_SCANNED, siteID);
					rsNotScanned = dbs.select(DBSelect.PAGES_NOT_SCANNED, siteID);
					rsAll.next();
					rsScanned.next();
					rsNotScanned.next();
					if (rs != null) {
						table_1DTM = LittleMethods.rsToDTM(rs, table_1Header);
						table_1.setModel(table_1DTM);
						table_1.setRowSorter(new TableRowSorter<DefaultTableModel>(table_1DTM));
					}
					table_2Object = new Object[][] {
						{"находятся в базе данных", rsAll == null ? "null" : rsAll.getString(1)},
						{"никогда не обходились, но находятся в базе данных", rsNotScanned == null ? "null" : rsNotScanned.getString(1)},
						{"Уже обходились", rsScanned == null ? "null" : rsScanned.getString(1)},
					};
				} catch (SQLException e2) {
					e2.printStackTrace();
				}

				table_2DTM = new DefaultTableModel(table_2Object, table_2Header);
				table_2.setModel(table_2DTM);
				lblLentaru.setText(comboBox.getSelectedItem().toString());
			}
		});
	}
}
