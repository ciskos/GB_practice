package io.gitlab.ciskos.gui.panels.adminTabs.tabElements;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.Main;
import io.gitlab.ciskos.db.DBDelete;
import io.gitlab.ciskos.db.DBInsert;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.db.DBUpdate;
import io.gitlab.ciskos.gui.panels.adminTabs.SitesTab;

public class SitesTabListeners {
	private JTextField textField;
	private JTextField textField_1;
	private JButton button;
	private JButton button_1;
	private JButton button_2;
	private JButton button_3;
	private JTextArea textArea;
	private JTable table;
	private String[] tableHeader;
	private DefaultTableModel tableDTM;

	public SitesTabListeners(SitesTab st) {
		this.textField = st.getTextField();
		this.textField_1 = st.getTextField_1();
		this.button = st.getButton();
		this.button_1 = st.getButton_1();
		this.button_2 = st.getButton_2();
		this.button_3 = st.getButton_3();
		this.textArea = st.getTextArea();
		this.table = st.getTable();
		this.tableHeader = st.getTableHeader();
		this.tableDTM = st.getTableDTM();
	}
	
	/**
	 * 
	 */
	public void button_3_AL() {
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String siteName = textField.getText();
				String url = textField_1.getText();
				String siteDescription = textArea.getText();
				String siteAdded = Main.getUserName() + " добавил сайт "
						+ siteName + " в таблицу \"sites\" ";
				DBInsert dbi = new DBInsert();
				DBSelect dbs = new DBSelect();
				ResultSet rsSiteList = null;
				ResultSet rsURLList = null;
				boolean siteAlreadyInBase = false;
				boolean urlAlreadyInBase = false;
				
				try {
					rsSiteList = dbs.select(DBSelect.SITES_LIST);
					rsURLList = dbs.select(DBSelect.PAGES_LIST);
					while (rsSiteList.next()) {
						if (rsSiteList.getString(2).equals(siteName)) {
							siteAlreadyInBase = true;
							break;
						}
					}
					while (rsURLList.next()) {
						if (rsURLList.getString(2).equals(url)) {
							urlAlreadyInBase = true;
							break;
						}
					}
				} catch (SQLException e2) {
					e2.printStackTrace();
				}

				if (!siteAlreadyInBase & !urlAlreadyInBase) {
					if (siteName.length() > 0 & url.length() > 0 & siteDescription.length() > 0) {
						try {
							JTable tlp = Main.getWindow().getlPanel().getTable();
							DefaultTableModel model = (DefaultTableModel) tlp.getModel();
							ResultSet rs = dbs.select(DBSelect.SITES_ID_FOR_PAGES, siteName);
							rs.next();
							String siteID = rs.getString(1);
							
							model.addRow(new Object[]{tlp.getRowCount() + 1, siteAdded});

							dbi.insertSite(siteName, siteDescription, Main.getUserID());
							dbi.insertPages(url, siteID);
							dbi.logAdd(siteAdded, Main.getUserID());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						textField.setText("");
						textField_1.setText("");
						textArea.setText("");
					} else {
						JOptionPane.showMessageDialog(null, "Заполните обязательные поля - Имя сайта, URL и Описание",
								"Ошибка", JOptionPane.OK_OPTION);
					} 
				} else {
					JOptionPane.showMessageDialog(null, "Такой сайт или ссылка уже есть",
							"Ошибка", JOptionPane.OK_OPTION);
				}
			}
		});
	}


	/**
	 * 
	 */
	public void button_2_AL() {
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				if (table.getSelectedRow() >= 0 && table.getValueAt(0, 0) != "") {
				if (table.getValueAt(0, 0) != null) {
					int dialogResult = JOptionPane.showConfirmDialog(null, "Действительно удалить?", "Вопрос",
							JOptionPane.YES_NO_OPTION);
					if (dialogResult == JOptionPane.YES_OPTION) {
						DBDelete dbd = new DBDelete();
						DBInsert dbi = new DBInsert();
						String siteDeleted = Main.getUserName() + " удалил сайт "
											+ table.getValueAt(table.getSelectedRow(), 1).toString()
											+ " из таблицы \"sites\" "; 
						String siteToDelete = table.getValueAt(table.getSelectedRow(), 0).toString();
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();

						model.addRow(new Object[]{tlp.getRowCount() + 1, siteDeleted});

						try {
							dbd.delete(DBDelete.DELETE_PERSONSPAGERANK_PAGES, siteToDelete);
							dbd.delete(DBDelete.DELETE_URL_FROM_PAGES, siteToDelete);
							dbd.delete(DBDelete.DELETE_SITE, siteToDelete);
							dbi.logAdd(siteDeleted, Main.getUserID());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}


	/**
	 * 
	 */
	public void button_1_AL() {
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() >= 0) {
					DBSelect dbs = new DBSelect();
					DBUpdate dbu = new DBUpdate();
					DBInsert dbi = new DBInsert();
					String siteToCheck = table.getValueAt(table.getSelectedRow(), 0).toString();
					String nameFromDB = null;
					String descriptionFromDB = null;
					String nameFromTable = table.getValueAt(table.getSelectedRow(), 1).toString();
					String descriptionFromTable = table.getValueAt(table.getSelectedRow(), 2).toString();
					String siteModified = Main.getUserName() + " обновил имя сайта "
							+ nameFromTable + " в таблице \"sites\" ";
					String descriptionModified = Main.getUserName() + " обновил описание сайта "
							+ descriptionFromTable + " в таблице \"sites\" ";

					try {
						ResultSet rs = dbs.select(DBSelect.SITES_CHECK_UPDATE, siteToCheck);
						rs.next();
						nameFromDB = rs.getNString("name");
						descriptionFromDB = rs.getNString("siteDescription");

						// TODO нужна проверка вводимых данных
						if (!nameFromDB.equals(nameFromTable)) {
							dbu.update(DBUpdate.UPDATE_SITES_NAME, siteToCheck, nameFromTable);
							dbi.logAdd(siteModified, Main.getUserID());
							JTable tlp = Main.getWindow().getlPanel().getTable();
							DefaultTableModel model = (DefaultTableModel) tlp.getModel();
							model.addRow(new Object[]{tlp.getRowCount() + 1, siteModified});
						}
						
						if (!descriptionFromDB.equals(descriptionFromTable)) {
							dbu.update(DBUpdate.UPDATE_SITES_DESCRIPTION, siteToCheck, descriptionFromTable);
							dbi.logAdd(descriptionModified, Main.getUserID());
							JTable tlp = Main.getWindow().getlPanel().getTable();
							DefaultTableModel model = (DefaultTableModel) tlp.getModel();
							model.addRow(new Object[]{tlp.getRowCount() + 1, descriptionModified});
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
	}


	/**
	 * 
	 */
	public void button_AL() {
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DBSelect dbs = new DBSelect();
				try {
					ResultSet rs = dbs.select(DBSelect.SITES, Main.getUserID());
					
					if (rs != null) {
						tableDTM = LittleMethods.rsToDTM(rs, tableHeader);

						table.setModel(tableDTM);
						table.setRowSorter(new TableRowSorter<DefaultTableModel>(tableDTM));
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
}
