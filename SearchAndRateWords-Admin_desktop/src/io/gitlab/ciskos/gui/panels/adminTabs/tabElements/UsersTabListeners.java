package io.gitlab.ciskos.gui.panels.adminTabs.tabElements;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import io.gitlab.ciskos.LittleMethods;
import io.gitlab.ciskos.Main;
import io.gitlab.ciskos.db.DBDelete;
import io.gitlab.ciskos.db.DBInsert;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.db.DBUpdate;
import io.gitlab.ciskos.gui.panels.adminTabs.UsersTab;

public class UsersTabListeners {
	
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private String[] tableHeader;
	private JButton button;
	private JButton button_1;
	private JButton button_2;
	private JButton button_3;
	private JButton button_4;
	private JCheckBox checkBox;
	private JTable table;
	private DefaultTableModel tableDTM;
	
	public UsersTabListeners(UsersTab ut) {
		this.textField = ut.getTextField();
		this.textField_1 = ut.getTextField_1();
		this.textField_2 = ut.getTextField_2();
		this.textField_4 = ut.getTextField_4();
		this.button = ut.getButton();
		this.button_1 = ut.getButton_1();
		this.button_2 = ut.getButton_2();
		this.button_3 = ut.getButton_3();
		this.button_4 = ut.getButton_4();
		this.checkBox = ut.getCheckBox();
		this.table = ut.getTable();
		this.tableHeader = ut.getTableHeader();
		this.tableDTM = ut.getTableDTM();
	}
	
	/**
	 * 
	 */
	public void button_4_AL() {
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() >= 0 && !table.getValueAt(0, 0).equals("")) {
					DBUpdate dbu = new DBUpdate();
					DBInsert dbi = new DBInsert();
					String userToUpdate = table.getValueAt(table.getSelectedRow(), 0).toString();
					String passwordModified = Main.getUserName() + " обновил пароль в \"users\" для пользователя "
							+ table.getValueAt(table.getSelectedRow(), 1).toString();

					if (!textField_4.getText().equals("")) {
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();
						
						model.addRow(new Object[] { tlp.getRowCount() + 1, passwordModified });
						textField_4.setText("");
						
						try {
							dbu.update(DBUpdate.UPDATE_USERS_PASSWORD, userToUpdate, textField_4.getText());
							dbi.logAdd(passwordModified, Main.getUserID());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					} else {
						JOptionPane.showMessageDialog(null, "Заполните поле нового пароля", "Ошибка",
								JOptionPane.OK_OPTION);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Выберите пользователя которому нужно изменить пароль",
							"Ошибка", JOptionPane.OK_OPTION);
				}
			}
		});
	}
	
	/**
	 * 
	 */
	public void button_3_AL() {
		button_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				String user = textField.getText();
				String password = textField_1.getText();
				String email = textField_2.getText();
				Boolean isAdmin = checkBox.isSelected();

				if (user.length() > 0 & password.length() > 0 & email.length() > 0 ) {
					try {
						DBInsert dbi = new DBInsert();
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();
						String userAdded = Main.getUserName() + " добавил нового пользователя "
								+ user + " в таблицу \"users\" "; 

						model.addRow(new Object[]{tlp.getRowCount() + 1, userAdded});

						dbi.insertUser(user, password, email, (isAdmin.toString().equals("true") ? 1:0), Main.getUserID());
						dbi.logAdd(userAdded, Main.getUserID());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					JOptionPane.showMessageDialog(null
							, "Заполните обязательные поля - Логин, Пароль и E-mail"
							, "Ошибка"
							, JOptionPane.OK_OPTION);
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_2_AL() {
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				if (table.getSelectedRow() >= 0 && table.getValueAt(0, 0) != "") {
				if (table.getValueAt(0, 0) != null) {
					int dialogResult = JOptionPane.showConfirmDialog(null, "Действительно удалить?", "Вопрос",
							JOptionPane.YES_NO_OPTION);
					if (dialogResult == JOptionPane.YES_OPTION) {
						DBDelete dbd = new DBDelete();
						DBInsert dbi = new DBInsert();
						String userToDelete = table.getValueAt(table.getSelectedRow(), 0).toString();
						JTable tlp = Main.getWindow().getlPanel().getTable();
						DefaultTableModel model = (DefaultTableModel) tlp.getModel();
						String userDeleted = Main.getUserName()
											+ " удалил из \"users\" пользователя "
											+ table.getValueAt(table.getSelectedRow(), 1).toString(); 

						model.addRow(new Object[]{tlp.getRowCount() + 1, userDeleted});

						try {
							dbd.delete(DBDelete.DELETE_USER, userToDelete);
							dbi.logAdd(userDeleted, Main.getUserID());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_1_AL() {
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() >= 0) {
					DBSelect dbs = new DBSelect();
					String userToCheck = (String)table.getValueAt(table.getSelectedRow(), 0);
					String loginFromTable = (String)table.getValueAt(table.getSelectedRow(), 1);
					String emailFromTable = (String)table.getValueAt(table.getSelectedRow(), 2);
					String isAdminFromTable = (String)table.getValueAt(table.getSelectedRow(), 3);
					
					// TODO нужна проверка вводимых данных
					try {
						ResultSet rs = dbs.select(DBSelect.USERS_CHECK_UPDATE, userToCheck);
						rs.next();
						String loginFromDB = rs.getString("login");
						String emailFromDB = rs.getString("email");
						String isAdminFromDB = rs.getString("isAdmin");
						String userModified = Main.getUserName() + " обновил \"users\" c " + loginFromDB + " на " + loginFromTable;
						String emailModified = Main.getUserName() + " обновил \"users\" c " + emailFromDB + " на " + emailFromTable;
						String isAdminModified = Main.getUserName() + " обновил \"users\" c " + isAdminFromDB + " на " + isAdminFromTable;
						
						checkAndLog(userToCheck, loginFromDB, loginFromTable, userModified);
						checkAndLog(userToCheck, emailFromDB, emailFromTable, emailModified);
						checkAndLog(userToCheck, isAdminFromDB, isAdminFromTable, isAdminModified);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void button_AL() {
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DBSelect dbs = new DBSelect();
				try {
					ResultSet rs = dbs.select(DBSelect.USERS, Main.getUserID());
					
					if (rs != null) {
						tableDTM = LittleMethods.rsToDTM(rs, tableHeader);

						table.setModel(tableDTM);
						table.setRowSorter(new TableRowSorter<DefaultTableModel>(tableDTM));
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * @param whatToCheck
	 * @param valueFromDB
	 * @param valueFromTable
	 * @param logMsg
	 * @throws SQLException
	 */
	private void checkAndLog(String whatToCheck, String valueFromDB, String valueFromTable, String logMsg)
			throws SQLException {
		if (!valueFromDB.equals(valueFromTable)) {
			DBUpdate dbu = new DBUpdate();
			DBInsert dbi = new DBInsert();
			JTable tlp = Main.getWindow().getlPanel().getTable();
			DefaultTableModel model = (DefaultTableModel) tlp.getModel();
			
			model.addRow(new Object[]{tlp.getRowCount() + 1, logMsg});

			dbu.update(DBUpdate.UPDATE_USERS_LOGIN, whatToCheck, valueFromTable);
			dbi.logAdd(logMsg, Main.getUserID());
		}
	}
}
