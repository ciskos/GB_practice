package io.gitlab.ciskos.gui.panels.logTabs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import io.gitlab.ciskos.Main;
import io.gitlab.ciskos.gui.MainWindow;

public class AdminToUserBtn extends JButton {

//	private JPanel adminPanel = Main.getWindow().getaPanel();
//	private JPanel userPanel = Main.getWindow().getuPanel();
//	private GroupLayout layout = Main.getWindow().getGroupLayout();
	private JPanel adminPanel;
	private JPanel userPanel;
	private GroupLayout layout;
	private MainWindow mWindow;

	
	private String adminToUser = "<html><center>Перейти в режим<br>пользователя</center></html>";
	private String userToAdmin = "<html><center>Вернуться в режим администратора</center></html>";
	
	/**
	 * @param text
	 */
//	public AdminToUserBtn(String text, JPanel adminPanel, JPanel userPanel, GroupLayout layout) {
	public AdminToUserBtn(String text, MainWindow mWindow) {
//	public AdminToUserBtn(String text) {
		super(text);
//		this.adminPanel = adminPanel;
//		this.userPanel = userPanel;
//		this.layout = layout;
		this.adminPanel = mWindow.getaPanel();
		this.userPanel = mWindow.getuPanel();
		this.layout = mWindow.getGroupLayout();
//		this.adminPanel = Main.getWindow().getaPanel();
//		this.userPanel = Main.getWindow().getuPanel();
//		this.layout = Main.getWindow().getGroupLayout();
		
		this.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (getText().equals(adminToUser)) {
					layout.replace(adminPanel, userPanel);
					setText(userToAdmin);
				} else {
					layout.replace(userPanel, adminPanel);
					setText(adminToUser);
				}
			}
		});
	}

}
