package io.gitlab.ciskos.gui.panels.logTabs;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class LogTable extends JTable {

	/**
	 * 
	 */
	public LogTable() {
		initialize();
	}

	/**
	 * 
	 */
	private void initialize() {
		this.setModel(new DefaultTableModel(
				new Object[][] {},
				new String[] {
						"№", "Действие"}
				) 
//		{
//			boolean[] columnEditables = new boolean[] {false, false};
//			public boolean isCellEditable(int row, int column) {
//				return columnEditables[column];}
//		}
	);
		this.getColumnModel().getColumn(0).setPreferredWidth(25);
	}

}
