/**
 * 
 */
package io.gitlab.ciskos.gui.panels.userTabs;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerDateModel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.json.JSONArray;
import org.json.JSONObject;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;

import io.gitlab.ciskos.db.DBConnect;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.db.ResultSetToJSON;

import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.RadialGradientPaint;
import java.awt.event.ActionEvent;

/**
 * @author user
 *
 */
public class DailyStatistics extends JPanel {

	private JTable table_3;
	private String[] table_3Header = new String[] {"№", "Дата", "Новых страниц"};
	private Object[][] table_3Object = new Object[][] {{null, null, null}};
	private DefaultTableModel table_3DTM = new DefaultTableModel(table_3Object,table_3Header);
	private JFreeChart chartBars = createChartBars(createDatasetBars(null));
	private JFreeChart chartPie = createChartPie(createDatasetPie(null));
	private ChartPanel panel_10 = new ChartPanel(chartPie);
    private ChartPanel panel_11 = new ChartPanel(chartBars);
	
	/**
	 * 
	 */
	public DailyStatistics() {
		GroupLayout gl_panel_3 = new GroupLayout(this);
		
		JButton button_1 = new JButton("Применить");
		JLabel label_6 = new JLabel("Данные за");
//		JLabel label_7 = new JLabel("Круговой график");
//		JLabel label_8 = new JLabel("График");
		JComboBox comboBox_2 = sitesComboBox();
		JComboBox comboBox_3 = personsComboBox();
//		JPanel panel_10 = new JPanel();
//		JPanel panel_11 = new JPanel();
		JScrollPane scrollPane_3 = new JScrollPane();
//		JSpinner spinner = new JSpinner();
		DatePickerSettings settingsFrom = new DatePickerSettings();
		settingsFrom.setFormatForDatesCommonEra(DateTimeFormatter.ISO_LOCAL_DATE);
		DatePickerSettings settingsTo = new DatePickerSettings();
		settingsTo.setFormatForDatesCommonEra(DateTimeFormatter.ISO_LOCAL_DATE);
		DatePicker dateFrom = new DatePicker(settingsFrom);
//		JSpinner spinner_1 = new JSpinner();
		DatePicker dateTo = new DatePicker(settingsTo);
		table_3 = new JTable();
		
		this.setLayout(gl_panel_3);

		table_3.setFillsViewportHeight(true);
		table_3.setModel(table_3DTM);
		table_3.getColumnModel().getColumn(0).setPreferredWidth(29);
		table_3.getColumnModel().getColumn(2).setPreferredWidth(94);
		
//		panel_10.add(label_7);
//		panel_11.add(label_8);
//		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"lenta.ru", "rambler.ru", "yandex.ru"}));
//		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"Путин", "Трамп", "Си Цзинпинь"}));
//		spinner.setModel(new SpinnerDateModel(new Date(1527800400000L), null, null, Calendar.DAY_OF_MONTH));
//		spinner_1.setModel(new SpinnerDateModel(new Date(1527800400000L), null, null, Calendar.DAY_OF_MONTH));
		scrollPane_3.setViewportView(table_3);
		
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// DB connection
				DBConnect dbc = new DBConnect();
				Connection con = dbc.connect();
				DBSelect dbs = new DBSelect(con);
				ResultSetToJSON rsToJSON = new ResultSetToJSON();
				String site = comboBox_2.getSelectedItem().toString();
				String person = comboBox_3.getSelectedItem().toString();
				String dFrom = dateFrom.getText().replaceAll("-", "");
				String dTo = dateTo.getText().replaceAll("-", "");
				
				if ("".equals(dFrom)) {
//					dFrom = LocalDate.now().toString().replaceAll("-", "");
					dFrom = "20000101";
					dateFrom.setDate(LocalDate.parse("2000-01-01"));
//					setText(dFrom);
				}
				if ("".equals(dTo)) {
//					dTo = LocalDate.now().toString().replaceAll("-", "");
					dTo = "21000101";
					dateTo.setDate(LocalDate.parse("2100-01-01"));
//					dateTo.setText(dTo);
				}
				ResultSet rs = dbs.statisticsByDate(site, person, dFrom, dTo);
				JSONArray ja = rsToJSON.toJSONArray(rs);
				
				dbc.disconnect();
				
				if (ja.toString() != null) {
					Object[][] tableObject = new Object[ja.length()][table_3Header.length];
					
					for (int i = 0; i < ja.length(); i++) {
							tableObject[i][0] = ja.getJSONObject(i).get("ID");;
							tableObject[i][1] = ja.getJSONObject(i).get("URL");
							tableObject[i][2] = ja.getJSONObject(i).get("Rank");
					}

					DefaultTableModel tableDTM = new DefaultTableModel(tableObject, table_3Header);

					table_3.setModel(tableDTM);
					table_3.setRowSorter(new TableRowSorter(tableDTM));
					
					chartBars = createChartBars(createDatasetBars(ja));
					chartPie = createChartPie(createDatasetPie(ja));
					panel_10.setChart(chartBars);
					panel_11.setChart(chartPie);
				}
			}
		});
		
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_3.createSequentialGroup()
							.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING, false)
								.addComponent(comboBox_2, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(dateFrom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING, false)
								.addComponent(dateTo, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox_3, Alignment.TRAILING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(8)
							.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE))
						.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane_3, GroupLayout.PREFERRED_SIZE, 227, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_10, GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
						.addComponent(panel_11, GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addGap(2)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_3.createSequentialGroup()
							.addComponent(panel_11, GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(panel_10, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
						.addGroup(gl_panel_3.createSequentialGroup()
							.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(button_1))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
								.addComponent(dateFrom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(dateTo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(label_6)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane_3, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)))
					.addContainerGap())
		);
	}

	private JComboBox<String> sitesComboBox() {
		JComboBox<String> comboBox_3 = new JComboBox<>();
		
		DBConnect dbc = new DBConnect();
		Connection con = dbc.connect();
		DBSelect dbs = new DBSelect(con);
		ResultSetToJSON rsToJSON = new ResultSetToJSON();
		ResultSet rs = dbs.sitesList();

		dbc.disconnect();

		int rows = 0;
		try {
			if (rs.last()) {
				rows = rs.getRow();
				rs.beforeFirst();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] sitesList = new String[rows];
		JSONArray ja = rsToJSON.toJSONArray(rs);
//		System.out.println(ja.getJSONObject(0).getString("name"));
		
		if (ja.toString() != null) {
			for (int i = 0; i < sitesList.length; i++) {
				sitesList[i] = ja.getJSONObject(i).getString("name");
			} 
		}
		
		comboBox_3.setModel(new DefaultComboBoxModel<String>(sitesList));
		return comboBox_3;
	}

	private JComboBox<String> personsComboBox() {
		JComboBox<String> comboBox_2 = new JComboBox<>();
		
		DBConnect dbc = new DBConnect();
		Connection con = dbc.connect();
		DBSelect dbs = new DBSelect(con);
		ResultSetToJSON rsToJSON = new ResultSetToJSON();
		ResultSet rs = dbs.personsList();

		dbc.disconnect();

		int rows = 0;
		try {
			if (rs.last()) {
				rows = rs.getRow();
				rs.beforeFirst();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] personsList = new String[rows];
		JSONArray ja = rsToJSON.toJSONArray(rs);
//		System.out.println(ja.getJSONObject(0).getString("name"));
		
		if (ja.toString() != null) {
			for (int i = 0; i < personsList.length; i++) {
				personsList[i] = ja.getJSONObject(i).getString("name");
			} 
		}
		
		comboBox_2.setModel(new DefaultComboBoxModel<String>(personsList));
		return comboBox_2;
	}
	
	private PieDataset createDatasetPie(JSONArray data) {
		DefaultPieDataset dataset = new DefaultPieDataset();
		if (data == null) {
			dataset.setValue("" , new Double(0));
		} else {
			for (int i = 0; i < data.length(); i++) {
				String url = (String) data.getJSONObject(i).get("URL");
				Double rank = Double.parseDouble(data.getJSONObject(i).get("Rank").toString());
				dataset.setValue(url, new Double(rank));
			}
		}
		return dataset;
	}

	private CategoryDataset createDatasetBars(JSONArray data) {
		if (data == null) return null;
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (int i = 0; i < data.length(); i++) {
			String url = (String) data.getJSONObject(i).get("URL");
			Double rank = Double.parseDouble(data.getJSONObject(i).get("Rank").toString());
			dataset.addValue(rank, url, "Страницы");
		}
		return dataset;
	}

	private JFreeChart createChartBars(CategoryDataset dataset)
	{
	    JFreeChart chart = ChartFactory.createBarChart(
	                      "Гистограмма упоминаний", 
	                      null,                   // x-axis label
	                      "Ранг",                // y-axis label
	                      dataset);
//	    chart.addSubtitle(new TextTitle("В доходе включен только " +
//	                                    "заработок по основной работе"));
	    chart.setBackgroundPaint(Color.white);

	    CategoryPlot plot = (CategoryPlot) chart.getPlot();

	    NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
	    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
	    BarRenderer renderer = (BarRenderer) plot.getRenderer();
	    renderer.setDrawBarOutline(false);
	    chart.getLegend().setFrame(BlockBorder.NONE);

	    return chart;
	}
	
	private JFreeChart createChartPie(PieDataset dataset)
	{
		Color[] colors = {new Color(200, 200, 255), new Color(255, 200, 200),
	            new Color(200, 255, 200), new Color(200, 255, 200)};
	    JFreeChart chart = ChartFactory.createPieChart(
	        "Страницы",  // chart title
	        dataset,             // data
	        false,               // no legend
	        true,                // tooltips
	        false                // no URL generation
	    );

	    // Определение фона графического изображения
//	    chart.setBackgroundPaint(new GradientPaint(new Point(0, 0), 
//	                                               new Color(20, 20, 20), 
//	                                               new Point(400, 200),
//	                                               Color.DARK_GRAY));
	    chart.setBackgroundPaint(Color.LIGHT_GRAY);


	    // Определение заголовка
	    TextTitle t = chart.getTitle();
	    t.setHorizontalAlignment(HorizontalAlignment.CENTER);
//	    t.setPaint(new Color(240, 240, 240));
	    t.setPaint(Color.BLACK);
	    t.setFont(new Font("Arial", Font.BOLD, 22));

	    // Определение подзаголовка
	    TextTitle source = new TextTitle("Вхождения по-страницам", 
	                                     new Font("Courier New", Font.PLAIN, 12));
	    source.setPaint(Color.BLACK);
	    source.setPosition(RectangleEdge.BOTTOM);
	    source.setHorizontalAlignment(HorizontalAlignment.RIGHT);
	    chart.addSubtitle(source);

	    PiePlot plot = (PiePlot) chart.getPlot();
	    plot.setBackgroundPaint(null);
	    plot.setInteriorGap(0.04);
	    plot.setOutlineVisible(false);

//	    RadialGradientPaint rgpBlue  ;
//	    RadialGradientPaint rgpRed   ;
//	    RadialGradientPaint rgpGreen ;
//	    RadialGradientPaint rgpYellow;
//
//	    rgpBlue   = createGradientPaint(colors[0], Color.BLUE  );
//	    rgpRed    = createGradientPaint(colors[1], Color.RED   );
//	    rgpGreen  = createGradientPaint(colors[2], Color.GREEN );
//	    rgpYellow = createGradientPaint(colors[3], Color.YELLOW);
		
//	     Определение секций круговой диаграммы
//	    plot.setSectionPaint("Оплата жилья" , rgpBlue  );
//	    plot.setSectionPaint("Школа, фитнес", rgpRed   );
//	    plot.setSectionPaint("Развлечения"  , rgpGreen );
//	    plot.setSectionPaint("Дача, стройка", rgpYellow);
//		
	    plot.setBaseSectionOutlinePaint(Color.BLACK);
	    plot.setSectionOutlinesVisible(true);
	    plot.setBaseSectionOutlineStroke(new BasicStroke(2.0f));

	    // Настройка меток названий секций
	    plot.setLabelFont(new Font("Courier New", Font.BOLD, 20));
	    plot.setLabelLinkPaint(Color.BLACK);
	    plot.setLabelLinkStroke(new BasicStroke(2.0f));
	    plot.setLabelOutlineStroke(null);
	    plot.setLabelPaint(Color.BLACK);
	    plot.setLabelBackgroundPaint(null);
	        
	    return chart;
	}
//	private RadialGradientPaint createGradientPaint(Color c1, Color c2)
//	{
//	    Point2D center = new Point2D.Float(0, 0);
//	    float radius = 200;
//	    float[] dist = {0.0f, 1.0f};
//	    return new RadialGradientPaint(center, radius, dist,
//	                                   new Color[] {c1, c2});
//	}
}
