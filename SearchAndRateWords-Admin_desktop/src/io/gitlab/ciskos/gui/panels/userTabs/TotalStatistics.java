/**
 * 
 */
package io.gitlab.ciskos.gui.panels.userTabs;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Point;
import java.awt.RadialGradientPaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.json.JSONArray;

import io.gitlab.ciskos.db.DBConnect;
import io.gitlab.ciskos.db.DBSelect;
import io.gitlab.ciskos.db.ResultSetToJSON;

/**
 * @author user
 *
 */
public class TotalStatistics extends JPanel{

	private JTable table_1;
	private JTable table_2;
	private String[] table_1Header = new String[] {"№", "Персона", "Упоминаний"};
	private Object[][] table_1Object = new Object[][] {{null, null, null}};
	private DefaultTableModel table_1DTM = new DefaultTableModel(table_1Object,	table_1Header);
	private String[] table_2Header = new String[] {"№", "Сайт", "Упоминаний"};
	private Object[][] table_2Object = new Object[][] {{null, null, null}};
	private DefaultTableModel table_2DTM = new DefaultTableModel(table_2Object, table_2Header);
	
//	private String[] comboBox_1Data = new String[] {"Путин", "Медведев", "Навальный"};
//	private DefaultComboBoxModel comboBox_1DCBM = new DefaultComboBoxModel(comboBox_1Data);
	private JFreeChart chartBars = createChartBars(createDatasetBars(null));
	private JFreeChart chartPie = createChartPie(createDatasetPie(null));
    private ChartPanel panel_6 = new ChartPanel(chartBars);
    private ChartPanel panel_7 = new ChartPanel(chartPie);
    private ChartPanel panel_8 = new ChartPanel(chartBars);
    private ChartPanel panel_9 = new ChartPanel(chartPie);

//    private JPanel panel_6 = new JPanel();
//    private JPanel panel_7 = new JPanel();
//	private JPanel panel_8 = new JPanel();
//	private JPanel panel_9 = new JPanel();
//	private JComboBox<String> comboBox_2;
    
	/**
	 * 
	 */
	public TotalStatistics() {
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		
		JLabel label =  new JLabel("Данные за ");
//		JLabel label_1 = new JLabel("График2");
//		JLabel label_2 = new JLabel("Круговой график");
		JLabel label_3 = new JLabel("Данные за ");
		JLabel label_4 = new JLabel("Круговой график");
		JLabel label_5 = new JLabel("График");
		JPanel panel_4 = new JPanel();
		JPanel panel_5 = new JPanel();
		JComboBox comboBox = sitesComboBox();
//		JComboBox comboBox_1 = new JComboBox();
		JComboBox comboBox_1 = personsComboBox();
		JButton button = new JButton("Применить");
		JButton btnNewButton_1 = new JButton("Применить");
		JScrollPane scrollPane_1 = new JScrollPane();
		JScrollPane scrollPane_2 = new JScrollPane();
		GroupLayout gl_panel_2 = new GroupLayout(this);
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		
		this.setLayout(gl_panel_2);
		
		table_1 = new JTable();
		table_2 = new JTable();
		
		table_1.setFillsViewportHeight(true);
		table_1.setModel(table_1DTM);
		table_2.setFillsViewportHeight(true);
		table_2.setModel(table_2DTM);
		
		scrollPane_1.setViewportView(table_1);
		scrollPane_2.setViewportView(table_2);

		panel_4.setLayout(gl_panel_4);
		panel_5.setLayout(gl_panel_5);
//		panel_8.add(label_4);
//		panel_9.add(label_5);
		
		tabbedPane_1.addTab("Сайты", null, panel_4, null);
		tabbedPane_1.addTab("Персоны", null, panel_5, null);
		
//		comboBox_1.setModel(comboBox_1DCBM);
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// DB connection
				DBConnect dbc = new DBConnect();
				Connection con = dbc.connect();
				DBSelect dbs = new DBSelect(con);
				ResultSetToJSON rsToJSON = new ResultSetToJSON();
				ResultSet rs = dbs.siteAllStatistics(comboBox.getSelectedItem().toString());
				JSONArray ja = rsToJSON.toJSONArray(rs);

//				System.out.println(comboBox.getSelectedItem());
				
				dbc.disconnect();
				
				if (ja.toString() != null) {
					Object[][] tableObject = new Object[ja.length()][table_1Header.length];
					
					for (int i = 0; i < ja.length(); i++) {
							tableObject[i][0] = ja.getJSONObject(i).get("ID");;
							tableObject[i][1] = ja.getJSONObject(i).get("name");
							tableObject[i][2] = ja.getJSONObject(i).get("rank");
					}

					DefaultTableModel tableDTM = new DefaultTableModel(tableObject, table_1Header);

					table_1.setModel(tableDTM);
					table_1.setRowSorter(new TableRowSorter(tableDTM));
					
					chartBars = createChartBars(createDatasetBars(ja));
					chartPie = createChartPie(createDatasetPie(ja));
					panel_6.setChart(chartBars);
					panel_7.setChart(chartPie);
				}
			}
		});

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// DB connection
				DBConnect dbc = new DBConnect();
				Connection con = dbc.connect();
				DBSelect dbs = new DBSelect(con);
				ResultSetToJSON rsToJSON = new ResultSetToJSON();
				ResultSet rs = dbs.personAllStatistics(comboBox_1.getSelectedItem().toString());
				JSONArray ja = rsToJSON.toJSONArray(rs);

//				System.out.println(comboBox.getSelectedItem());
				
				dbc.disconnect();
				
				if (ja.toString() != null) {
					Object[][] tableObject = new Object[ja.length()][table_1Header.length];
					
					for (int i = 0; i < ja.length(); i++) {
							tableObject[i][0] = ja.getJSONObject(i).get("ID");;
							tableObject[i][1] = ja.getJSONObject(i).get("name");
							tableObject[i][2] = ja.getJSONObject(i).get("rank");
					}

					DefaultTableModel tableDTM = new DefaultTableModel(tableObject, table_2Header);

					table_2.setModel(tableDTM);
					table_2.setRowSorter(new TableRowSorter(tableDTM));
					
//					System.out.println(Arrays.deepToString(tableObject));
					System.out.println(tableObject[0][2]);
					
					if (!tableObject[0][2].equals("null")) {
						chartBars = createChartBars(createDatasetBars(ja));
						chartPie = createChartPie(createDatasetPie(ja));
						panel_8.setChart(chartBars);
						panel_9.setChart(chartPie);
					}
				}			
			}
		});

		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, gl_panel_4.createSequentialGroup()
							.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_1))
						.addComponent(label, Alignment.LEADING)
						.addComponent(scrollPane_1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 227, GroupLayout.PREFERRED_SIZE))
					.addGap(10)
					.addGroup(gl_panel_4.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_7, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_4.createSequentialGroup()
							.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(panel_7, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
						.addGroup(gl_panel_4.createSequentialGroup()
							.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_1))
							.addGap(18)
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)))
					.addContainerGap())
		);

		gl_panel_5.setHorizontalGroup(
				gl_panel_5.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_5.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
							.addGroup(Alignment.LEADING, gl_panel_5.createSequentialGroup()
								.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(button))
							.addComponent(label_3, Alignment.LEADING)
							.addComponent(scrollPane_2, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 227, GroupLayout.PREFERRED_SIZE))
						.addGap(10)
						.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
							.addComponent(panel_8, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(panel_9, GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE))
						.addContainerGap())
			);
			gl_panel_5.setVerticalGroup(
				gl_panel_5.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel_5.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel_5.createSequentialGroup()
								.addComponent(panel_8, GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
								.addGap(18)
								.addComponent(panel_9, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
							.addGroup(gl_panel_5.createSequentialGroup()
								.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
									.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(button))
								.addGap(18)
								.addComponent(label_3)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)))
						.addContainerGap())
			);

		
//		gl_panel_5.setHorizontalGroup(
//				gl_panel_5.createParallelGroup(Alignment.LEADING)
//					.addGroup(gl_panel_5.createSequentialGroup()
//						.addContainerGap()
//						.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
//							.addGroup(gl_panel_5.createSequentialGroup()
//								.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
//								.addGap(6)
//								.addComponent(button, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE))
//							.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
//							.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 227, GroupLayout.PREFERRED_SIZE))
//						.addGap(10)
//						.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
//							.addComponent(panel_8, GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
//							.addComponent(panel_9, GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE))
//						.addContainerGap())
//			);
//			gl_panel_5.setVerticalGroup(
//				gl_panel_5.createParallelGroup(Alignment.LEADING)
//					.addGroup(gl_panel_5.createSequentialGroup()
//						.addGap(1)
//						.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
//							.addGroup(gl_panel_5.createSequentialGroup()
//								.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
//									.addGroup(gl_panel_5.createSequentialGroup()
//										.addGap(1)
//										.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
//									.addComponent(button))
//								.addGap(18)
//								.addComponent(label_3)
//								.addPreferredGap(ComponentPlacement.RELATED)
//								.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE))
//							.addGroup(Alignment.TRAILING, gl_panel_5.createSequentialGroup()
//								.addComponent(panel_9, GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE)
//								.addGap(18)
//								.addComponent(panel_8, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)))
//						.addContainerGap())
//			);
		
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane_1, GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane_1, GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
					.addContainerGap())
		);
	}

	private JComboBox<String> sitesComboBox() {
		JComboBox<String> comboBox_3 = new JComboBox<>();
		
		DBConnect dbc = new DBConnect();
		Connection con = dbc.connect();
		DBSelect dbs = new DBSelect(con);
		ResultSetToJSON rsToJSON = new ResultSetToJSON();
		ResultSet rs = dbs.sitesList();

		dbc.disconnect();

		int rows = 0;
		try {
			if (rs.last()) {
				rows = rs.getRow();
				rs.beforeFirst();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] sitesList = new String[rows];
		JSONArray ja = rsToJSON.toJSONArray(rs);
//		System.out.println(ja.getJSONObject(0).getString("name"));
		
		if (ja.toString() != null) {
			for (int i = 0; i < sitesList.length; i++) {
				sitesList[i] = ja.getJSONObject(i).getString("name");
			} 
		}
		
		comboBox_3.setModel(new DefaultComboBoxModel<String>(sitesList));
		return comboBox_3;
	}

	private JComboBox<String> personsComboBox() {
		JComboBox<String> comboBox_2 = new JComboBox<>();
		
		DBConnect dbc = new DBConnect();
		Connection con = dbc.connect();
		DBSelect dbs = new DBSelect(con);
		ResultSetToJSON rsToJSON = new ResultSetToJSON();
		ResultSet rs = dbs.personsList();

		dbc.disconnect();

		int rows = 0;
		try {
			if (rs.last()) {
				rows = rs.getRow();
				rs.beforeFirst();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] personsList = new String[rows];
		JSONArray ja = rsToJSON.toJSONArray(rs);
//		System.out.println(ja.getJSONObject(0).getString("name"));
		
		if (ja.toString() != null) {
			for (int i = 0; i < personsList.length; i++) {
				personsList[i] = ja.getJSONObject(i).getString("name");
			} 
		}
		
		comboBox_2.setModel(new DefaultComboBoxModel<String>(personsList));
		return comboBox_2;
	}

	private PieDataset createDatasetPie(JSONArray data) {
		DefaultPieDataset dataset = new DefaultPieDataset();
		if (data == null) {
			dataset.setValue("" , new Double(0));
		} else {
			for (int i = 0; i < data.length(); i++) {
				String name = (String) data.getJSONObject(i).get("name");
				Double rank = Double.parseDouble(data.getJSONObject(i).get("rank").toString());
				dataset.setValue(name, new Double(rank));
			}
		}
		return dataset;
	}

	private CategoryDataset createDatasetBars(JSONArray data) {
		if (data == null) return null;
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (int i = 0; i < data.length(); i++) {
			String name = (String) data.getJSONObject(i).get("name");
			Double rank = Double.parseDouble(data.getJSONObject(i).get("rank").toString());
			dataset.addValue(rank, name, "Персоны");
		}
		return dataset;
	}

	private JFreeChart createChartBars(CategoryDataset dataset)
	{
	    JFreeChart chart = ChartFactory.createBarChart(
	                      "Гистограмма персон", 
	                      null,                   // x-axis label
	                      "Ранг",                // y-axis label
	                      dataset);
//	    chart.addSubtitle(new TextTitle("В доходе включен только " +
//	                                    "заработок по основной работе"));
	    chart.setBackgroundPaint(Color.white);

	    CategoryPlot plot = (CategoryPlot) chart.getPlot();

	    NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
	    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
	    BarRenderer renderer = (BarRenderer) plot.getRenderer();
	    renderer.setDrawBarOutline(false);
	    chart.getLegend().setFrame(BlockBorder.NONE);

	    return chart;
	}
	
	private JFreeChart createChartPie(PieDataset dataset)
	{
		Color[] colors = {new Color(200, 200, 255), new Color(255, 200, 200),
	            new Color(200, 255, 200), new Color(200, 255, 200)};
	    JFreeChart chart = ChartFactory.createPieChart(
	        "Персоны",  // chart title
	        dataset,             // data
	        false,               // no legend
	        true,                // tooltips
	        false                // no URL generation
	    );

	    // Определение фона графического изображения
//	    chart.setBackgroundPaint(new GradientPaint(new Point(0, 0), 
//	                                               new Color(20, 20, 20), 
//	                                               new Point(400, 200),
//	                                               Color.DARK_GRAY));
	    chart.setBackgroundPaint(Color.LIGHT_GRAY);


	    // Определение заголовка
	    TextTitle t = chart.getTitle();
	    t.setHorizontalAlignment(HorizontalAlignment.CENTER);
//	    t.setPaint(new Color(240, 240, 240));
	    t.setPaint(Color.BLACK);
	    t.setFont(new Font("Arial", Font.BOLD, 22));

	    // Определение подзаголовка
	    TextTitle source = new TextTitle("Соотношение персон", 
	                                     new Font("Courier New", Font.PLAIN, 12));
	    source.setPaint(Color.BLACK);
	    source.setPosition(RectangleEdge.BOTTOM);
	    source.setHorizontalAlignment(HorizontalAlignment.RIGHT);
	    chart.addSubtitle(source);

	    PiePlot plot = (PiePlot) chart.getPlot();
	    plot.setBackgroundPaint(null);
	    plot.setInteriorGap(0.04);
	    plot.setOutlineVisible(false);

//	    RadialGradientPaint rgpBlue  ;
//	    RadialGradientPaint rgpRed   ;
//	    RadialGradientPaint rgpGreen ;
//	    RadialGradientPaint rgpYellow;
//
//	    rgpBlue   = createGradientPaint(colors[0], Color.BLUE  );
//	    rgpRed    = createGradientPaint(colors[1], Color.RED   );
//	    rgpGreen  = createGradientPaint(colors[2], Color.GREEN );
//	    rgpYellow = createGradientPaint(colors[3], Color.YELLOW);
		
//	     Определение секций круговой диаграммы
//	    plot.setSectionPaint("Оплата жилья" , rgpBlue  );
//	    plot.setSectionPaint("Школа, фитнес", rgpRed   );
//	    plot.setSectionPaint("Развлечения"  , rgpGreen );
//	    plot.setSectionPaint("Дача, стройка", rgpYellow);
//		
	    plot.setBaseSectionOutlinePaint(Color.BLACK);
	    plot.setSectionOutlinesVisible(true);
	    plot.setBaseSectionOutlineStroke(new BasicStroke(2.0f));

	    // Настройка меток названий секций
	    plot.setLabelFont(new Font("Courier New", Font.BOLD, 20));
	    plot.setLabelLinkPaint(Color.BLACK);
	    plot.setLabelLinkStroke(new BasicStroke(2.0f));
	    plot.setLabelOutlineStroke(null);
	    plot.setLabelPaint(Color.BLACK);
	    plot.setLabelBackgroundPaint(null);
	        
	    return chart;
	}
	private RadialGradientPaint createGradientPaint(Color c1, Color c2)
	{
	    Point2D center = new Point2D.Float(0, 0);
	    float radius = 200;
	    float[] dist = {0.0f, 1.0f};
	    return new RadialGradientPaint(center, radius, dist,
	                                   new Color[] {c1, c2});
	}
}
